# AI Trainer


A Flutter app that counts repetitions and measure time of exercises in real time from the camera.
Social network of exercises. You can upload exercise and others users can train yours exercise  
Use Firebase as a backend.
Use Posenet for pose estimation


## Install 

```
flutter packages get
```

## Run

```
flutter run
```

## Images

### Exercises

![s_5](/uploads/5fba335a2f2f6ac8f4088a4da08c0303/s_5.jpg)

### View Exercises Stats

![s_1](/uploads/75c97709b21bb9422442552fe74f2c03/s_1.jpg)

### Upload Exercises

![s_2](/uploads/abea0ae28790b5ebc1a0703624fd0f29/s_2.jpg)

### Upload Exercises

![s_3](/uploads/95b58c8ca2e2407b3cb18c7d068d80e5/s_3.jpg)

### Train Exercises

![s_4](/uploads/7017c8d22aa384c7660215bab631449c/s_4.jpg)
