import 'dart:async';

import 'package:camera/camera.dart';
import 'package:aiTrainer/domain/entities/camera_data.dart';
import 'package:aiTrainer/domain/entities/cameras.dart';
import 'package:aiTrainer/domain/entities/frame.dart';
import 'package:aiTrainer/domain/entities/rect.dart';

abstract class CameraSource {
  void setCamera(CameraData camera);
  Future<Cameras> getCamerasOptions();
  Future<void> init(Function(Frame) onFind);
  Future<CameraController> getController();
  int getNumberOfCameras();
  Future<void> stopFrameStreaming();
}

// TODO: check for erros
// TODO: use gpu for the tflite

class CameraSourceImp implements CameraSource {
  CameraData selectedCamera;
  Map<String, CameraDescription> camerasMap;
  Cameras camerasOptions = new Cameras();
  CameraController controller;

  CameraSourceImp() {
    camerasMap = new Map<String, CameraDescription>();
  }

  void initCameraControllerIfNull() {
    if (controller == null) {
      controller = new CameraController(
          camerasMap[selectedCamera.cameraName], ResolutionPreset.veryHigh);
    }
  }

  @override
  int getNumberOfCameras() {
    return this.camerasOptions.cameras.length;
  }

  @override
  Future<Cameras> getCamerasOptions() async {
    if (camerasMap.isEmpty) {
      List<CameraDescription> cameras = await availableCameras();
      for (var camera in cameras) {
        camerasMap[camera.lensDirection.toString()] = camera;
        var newCameraData = CameraData(camera.lensDirection.toString());
        selectedCamera = newCameraData;
        camerasOptions.cameras.add(newCameraData);
      }
    }
    return camerasOptions;
  }

  @override
  void setCamera(CameraData camera) {
    selectedCamera = camera;
    controller = null;
  }

  @override
  Future<void> init(Function(Frame) onCapture) async {
    await getCamerasOptions();
    initCameraControllerIfNull();
    await controller.initialize();
    controller.startImageStream((CameraImage img) {
      Frame frame = new Frame(
          img, RectImage(img.height.toDouble(), img.width.toDouble()));
      onCapture(frame);
    });
  }

  @override
  Future<CameraController> getController() async {
    return this.controller;
  }

  @override
  Future<void> stopFrameStreaming() {
    return controller.stopImageStream();
    this.controller = null;
  }
}
