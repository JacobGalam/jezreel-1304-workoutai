import 'dart:async';

import 'package:aiTrainer/domain/entities/login_data.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class FirebaseAuthInteface {
  FirebaseAuth _auth = FirebaseAuth.instance;
  GoogleSignIn _googleSignIn = GoogleSignIn();

  Future<LoginData> getLoagedData() async {
    var user = _auth.currentUser;
    if (user == null || user.isAnonymous) {
      return null;
    }
    return LoginData(user.displayName, user.photoURL);
  }

  /// return id even if the user if not login already. (signInAnonymously)
  Future<String> getUserId() async {
    var user = _auth.currentUser;

    if (user == null) {
      var userCredentials = await _auth.signInAnonymously();
      return userCredentials.user.uid;
    }

    return user.uid;
  }

  Future<LoginData> login() async {
    final GoogleSignInAccount googleSignInAccount =
        await _googleSignIn.signIn();
    if (googleSignInAccount == null) {
      return null;
    }
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final UserCredential authResult =
        await _auth.signInWithCredential(credential);
    final User user = authResult.user;

    if (user != null) {
      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      final User currentUser = _auth.currentUser;
      assert(user.uid == currentUser.uid);

      return LoginData(user.displayName, user.photoURL);
    }

    return null;
  }

  Future<void> logout() {
    return _googleSignIn.signOut();
  }
}
