import 'dart:async';

import 'package:aiTrainer/data/models/posenet_regeneration.dart';
import 'package:aiTrainer/domain/entities/frame.dart';
import 'package:aiTrainer/domain/entities/rect.dart';
import 'package:tflite/tflite.dart';

import 'package:aiTrainer/defs.dart' as defs;

abstract class PosenetSource {
  Future<void> init();
  Future<PosenetRegeneration> recognize(Frame frame, RectImage wantedSize);
  Future<PosenetRegeneration> recognizeFile(String path);
}

class PosenetSourceImp implements PosenetSource {
  bool isDetecting = false;
  bool isInited = false;

  // TODO: add wanted size
  @override
  Future<void> init() async {
    if (isInited) {
      return;
    }
    String res;
    // TODO: run on GPU and more thered and shit
    res = await Tflite.loadModel(
      useGpuDelegate: defs.runPoseNetOnGpu,
      numThreads: 4,
      model: "assets/posenet_mv1_075_float_from_checkpoints.tflite",
    );
    print(res); // TODO: remove
    isInited = true;
  }

  @override
  Future<PosenetRegeneration> recognize(
      Frame frame, RectImage wantedSize) async {
    if (!isDetecting) {
      isDetecting = true;
      int startTime = new DateTime.now().millisecondsSinceEpoch;
      var img = frame.cameraFrame;

      // var recognitions = await Tflite.runPoseNetOnFrame(
      //   bytesList: img.planes.map((plane) {
      //     return plane.bytes;
      //   }).toList(),
      //   imageHeight: img.height,
      //   imageWidth: img.width,
      //   numResults: 1,
      // );
      var recognitions = await Tflite.runPoseNetOnFrame(
        bytesList: img.planes.map((plane) {
          return plane.bytes;
        }).toList(), // required
        imageHeight: img.height,
        imageWidth: img.width,
        numResults: 1,
        rotation: -90,
        threshold: 0.3,
        nmsRadius: 10,
      );

      // TODO: check if it works

      int endTime = new DateTime.now().millisecondsSinceEpoch;
      print("Detection took ${endTime - startTime}");
      isDetecting = false;
      if (recognitions.length > 0) {
        var a = new PosenetRegeneration(recognitions, frame.size, wantedSize);
        return a;
      }
    }
    return null;
  }

  @override
  Future<PosenetRegeneration> recognizeFile(String path) async {
    int startTime = new DateTime.now().millisecondsSinceEpoch;

    var results = await Tflite.runPoseNetOnImage(
      path: path,
      numResults: 1,
      // TODO: check if need this field: threshold: 0.1,
    );

    int endTime = new DateTime.now().millisecondsSinceEpoch;
    print("Detection took ${endTime - startTime}");

    if (results.length > 0) {
      return PosenetRegeneration(results, null, null);
    } else {
      return null;
    }
  }
}
