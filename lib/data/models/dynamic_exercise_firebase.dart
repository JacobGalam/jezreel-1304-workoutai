import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:aiTrainer/domain/entities/dynamic_exercise.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/domain/entities/exercise_value.dart';

class DynamicExerciseFirebase extends DynamicExercise {
  DynamicExerciseFirebase.fromData(DynamicExercise data) {
    this.exerciseValue = data.exerciseValue;
    this.imagePath = data.imagePath;
    this.info = data.info;
    this.name = data.name;
    this.type = data.type;
    this.owner = data.owner;
  }

  Map<String, dynamic> toMap() {
    return {
      'up': exerciseValue.up,
      'down': exerciseValue.down,
      'part': exerciseValue.part,
      'imageUrl': this.imagePath,
      'info': this.info,
      'TitleName': this.name,
      'indexName': this.name.toLowerCase(),
      'type': exerciesTypeToString(this.type),
      'owner': this.owner,
    };
  }

  DynamicExerciseFirebase.fromMap(QueryDocumentSnapshot map, String id) {
    this.id = id;
    this.exerciseValue = new ExerciseValue(
      down: map['down'],
      up: map['up'],
      part: map['part'],
    );
    this.imagePath = map['imageUrl'];
    this.info = map['info'];
    this.name = map['TitleName'];
    this.type = exerciesTypeFromString(map['type']);
    this.owner = map['owner'];
  }
}
