import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/domain/entities/exersice_count_history.dart';
import 'package:aiTrainer/domain/entities/exersice_history.dart';

class ExersiceHistoryFirebase {
  String value;
  DateTime start;
  DateTime end;
  String exersiceName;
  String owner;
  ExerciesType type;
  String exersiceId;

  ExersiceHistoryFirebase(QueryDocumentSnapshot map) {
    this.value = map['value'];
    this.start = DateTime.parse(map['start']);
    this.end = DateTime.parse(map['end']);
    this.exersiceId = map['exersiceId'];
    this.owner = map['owner'];
    this.type = exerciesTypeFromString(map['type']);
  }

  ExersiceHistoryFirebase.fromData(
      ExersiceHistory history, String owner, String exersiceId) {
    this.end = history.end;
    this.start = history.start;
    this.exersiceName = history.exersiceName;
    this.value = history.valueAsString();
    this.owner = owner;
    this.exersiceId = exersiceId;
    this.type = history.type;
  }

  Map<String, dynamic> toMap() {
    return {
      'value': this.value,
      'start': this.start.toString(),
      'end': this.end.toString(),
      'exersiceId': this.exersiceId,
      'owner': this.owner,
      'type': exerciesTypeToString(this.type),
    };
  }
}
