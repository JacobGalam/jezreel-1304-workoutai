import 'package:aiTrainer/domain/entities/angle_math.dart';
import 'package:aiTrainer/domain/entities/key_points.dart';
import 'package:aiTrainer/domain/entities/point.dart';
import 'package:aiTrainer/domain/entities/recognizing.dart';
import 'package:aiTrainer/domain/entities/rect.dart';

class PosenetRegeneration extends Recognizing {
  RectImage wantedSize;

  @override
  RectImage imageSize;
  @override
  bool found;

  @override
  double leftChest;
  @override
  double leftElbow;
  @override
  double leftHip;
  @override
  double leftKnee;
  @override
  double leftShoulder;
  @override
  double neck;
  @override
  double rightChest;
  @override
  double rightElbow;
  @override
  double rightHip;
  @override
  double rightKnee;
  @override
  double rightShoulder;

  // TODO: move it to general function
  Point makePointReletiveToScreen(
      Point oldPoint, RectImage size, RectImage wantedSize) {
    var scaleW, scaleH, x, y;
    Point returnPoint = new Point();
    // if (wantedSize.width / wantedSize.width > size.height / size.width) {
    //   scaleW = wantedSize.width / size.height * size.width;
    //   scaleH = wantedSize.width;
    //   var difW = (scaleW - wantedSize.width) / scaleW;
    //   x = (oldPoint.x - difW / 2) * scaleW;
    //   y = oldPoint.y * scaleH;
    // } else {
    //   scaleH = wantedSize.width / size.width * size.height;
    //   scaleW = wantedSize.width;
    //   var difH = (scaleH - wantedSize.width) / scaleH;
    //   x = oldPoint.x * scaleW;
    //   y = (oldPoint.y - difH / 2) * scaleH;
    // }
    returnPoint.x = oldPoint.x;
    returnPoint.y = oldPoint.y;
    return returnPoint;
  }

  PosenetRegeneration(
      List<dynamic> recognitions, RectImage size, RectImage wantedSize) {
    this.imageSize = size;
    this.keyPointsModel.raw = recognitions;
    this.wantedSize = wantedSize;
    if (recognitions.length > 0) {
      this.found = true;
      var firstPerson = recognitions[0];

      readPoseKeyPoints(firstPerson);
      calculateAngles();
    } else {
      this.found = false;
    }
  }

  void readPoseKeyPoints(firstPerson) {
    for (var part in firstPerson["keypoints"].values) {
      Point place = new Point();
      place.x = part["x"];
      place.y = part["y"];
      Point newPoint =
          makePointReletiveToScreen(place, this.imageSize, this.wantedSize);

      KeyPointModel keyPoint = new KeyPointModel();
      keyPoint.xPoint = newPoint.x;
      keyPoint.yPoint = newPoint.y;
      switch (part["part"]) {
        case "nose":
          this.keyPointsModel.nose = keyPoint;
          break;
        case "leftEye":
          this.keyPointsModel.leftEye = keyPoint;
          break;
        case "rightEye":
          this.keyPointsModel.rightEye = keyPoint;
          break;
        case "leftEar":
          this.keyPointsModel.leftEar = keyPoint;
          break;
        case "rightEar":
          this.keyPointsModel.rightEar = keyPoint;
          break;
        case "leftShoulder":
          this.keyPointsModel.leftShoulder = keyPoint;
          break;
        case "rightShoulder":
          this.keyPointsModel.rightShoulder = keyPoint;
          break;
        case "leftElbow":
          this.keyPointsModel.leftElbow = keyPoint;
          break;
        case "rightElbow":
          this.keyPointsModel.rightElbow = keyPoint;
          break;
        case "leftWrist":
          this.keyPointsModel.leftWrist = keyPoint;
          break;
        case "rightWrist":
          this.keyPointsModel.rightWrist = keyPoint;
          break;
        case "leftHip":
          this.keyPointsModel.leftHip = keyPoint;
          break;
        case "rightHip":
          this.keyPointsModel.rightHip = keyPoint;
          break;
        case "leftKnee":
          this.keyPointsModel.leftKnee = keyPoint;
          break;
        case "rightKnee":
          this.keyPointsModel.rightKnee = keyPoint;
          break;
        case "leftAnkle":
          this.keyPointsModel.leftAnkle = keyPoint;
          break;
        case "rightAnkle":
          this.keyPointsModel.rightAnkle = keyPoint;
          break;
        default:
          throw ("There is no x,y avaliablle.");
      }
    }
  }

  void calculateAngles() {
    var m = this.keyPointsModel;

    this.neck = calculateAngle(m.nose, m.rightShoulder, m.leftShoulder);

    this.rightShoulder =
        calculateAngle(m.rightShoulder, m.rightElbow, m.rightHip);
    this.leftShoulder = calculateAngle(m.leftShoulder, m.leftElbow, m.leftHip);

    this.rightChest =
        calculateAngle(m.rightShoulder, m.rightHip, m.leftShoulder);
    this.leftChest = calculateAngle(m.leftShoulder, m.leftHip, m.rightShoulder);

    this.rightElbow =
        calculateAngle(m.rightElbow, m.rightWrist, m.rightShoulder);
    this.leftElbow = calculateAngle(m.leftElbow, m.leftWrist, m.leftShoulder);

    this.rightHip = calculateAngle(m.rightHip, m.rightKnee, m.leftHip);
    this.leftHip = calculateAngle(m.leftHip, m.leftKnee, m.rightHip);

    this.rightKnee = calculateAngle(m.rightKnee, m.rightHip, m.rightAnkle);
    this.leftKnee = calculateAngle(m.leftKnee, m.leftHip, m.leftAnkle);
  }
}
