import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:aiTrainer/data/datasources/posenet_souce.dart';
import 'package:aiTrainer/domain/entities/frame.dart';
import 'package:aiTrainer/domain/entities/recognizing.dart';
import 'package:aiTrainer/domain/repositories/ai_recognition_pose.dart';

// TODO: move the logic from the sourse to this class

class AiRecognitionPoseImp implements AiRecognitionPose {
  PosenetSource posenetSource;

  AiRecognitionPoseImp({
    @required this.posenetSource,
  });

  Future<void> init() {
    return posenetSource.init();
  }

  Future<Recognizing> recognize(Frame frame) async {
    return await posenetSource.recognize(frame, null);
  }
}
