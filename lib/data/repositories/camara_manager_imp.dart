import 'dart:async';

import 'package:camera/camera.dart';
import 'package:aiTrainer/data/datasources/camera_source.dart';
import 'package:aiTrainer/domain/entities/cameras.dart';
import 'package:aiTrainer/domain/entities/frame.dart';
import 'package:aiTrainer/domain/entities/rect.dart';
import 'package:aiTrainer/domain/repositories/camara_manager.dart';

class CameraManagerImp implements CameraManager {
  CameraSource _cameraSource;

  Function(Frame) onImageCallback;

  Cameras options;
  RectImage wantedSize;
  int currentCamera = 0;

  CameraManagerImp(this._cameraSource);

  @override
  Future<void> init(Function(Frame) callback, RectImage wantedSize) async {
    this.wantedSize = wantedSize;
    this.onImageCallback = callback;
    await this._cameraSource.init(onCaptureFrame);
    options = await this._cameraSource.getCamerasOptions();
  }

  void onCaptureFrame(Frame frame) async {
    this.onImageCallback(frame);
  }

  @override
  Future<CameraController> getController() async {
    return this._cameraSource.getController();
  }

  @override
  bool haveAlsoBackCamera() {
    return this._cameraSource.getNumberOfCameras() > 1;
  }

  @override
  Future<CameraController> switchCamera() async {
    currentCamera = currentCamera == 0 ? 1 : 0;
    this._cameraSource.setCamera(options.cameras[currentCamera]);
    await this._cameraSource.init(onCaptureFrame);
    return await this._cameraSource.getController();
  }

  @override
  Future<void> stopFrameStreaming() {
    return this._cameraSource.stopFrameStreaming();
  }
}
