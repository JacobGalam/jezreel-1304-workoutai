import 'dart:async';

import 'package:aiTrainer/data/datasources/firebase_auth_inteface.dart';
import 'package:aiTrainer/domain/entities/login_data.dart';
import 'package:aiTrainer/domain/repositories/credentials_repostory.dart';

class FirebaseCredentials implements CredentialsRepostory {
  FirebaseAuthInteface firebaseAuth;

  FirebaseCredentials(this.firebaseAuth);

  // return null if not login
  @override
  Future<LoginData> getLoagedData() async {
    return firebaseAuth.getLoagedData();
  }

  @override
  Future<LoginData> login() async {
    return firebaseAuth.login();
  }

  @override
  Future<void> logout() {
    return firebaseAuth.logout();
  }

  @override
  Future<String> getUserId() {
    return firebaseAuth.getUserId();
  }
}
