import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:aiTrainer/data/models/exersice_history_firebase.dart';
import 'package:aiTrainer/domain/entities/exercise_data.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/domain/entities/exersice_count_history.dart';
import 'package:aiTrainer/domain/entities/exersice_history.dart';
import 'package:aiTrainer/domain/entities/exersice_time_history.dart';
import 'package:aiTrainer/domain/repositories/credentials_repostory.dart';
import 'package:aiTrainer/domain/repositories/history.dart';

class FirebaseHistory implements History {
  CredentialsRepostory firebaseAuth;

  FirebaseHistory({@required this.firebaseAuth});

  ExersiceHistory _docToHistory(QueryDocumentSnapshot doc) {
    var histoyFireBase = ExersiceHistoryFirebase(doc);

    if (histoyFireBase.type == ExerciesType.time) {
      var time = Duration(milliseconds: int.parse(histoyFireBase.value));
      return ExersiceTimeHistory(
          end: histoyFireBase.end,
          start: histoyFireBase.start,
          exersiceName: histoyFireBase.exersiceName,
          time: time);
    } else if (histoyFireBase.type == ExerciesType.count) {
      return ExersiceCountHistory(
          end: histoyFireBase.end,
          start: histoyFireBase.start,
          exersiceName: histoyFireBase.exersiceName,
          reps: int.parse(histoyFireBase.value));
    } else {
      throw new Exception("unknown type");
    }
  }

  Map<String, dynamic> _historyToMap(
      ExersiceHistory history, String userId, String exerciseId) {
    var histoyFireBase =
        ExersiceHistoryFirebase.fromData(history, userId, exerciseId);
    return histoyFireBase.toMap();
  }

  @override
  Future<List<ExersiceHistory>> getAllExersiceHistory() async {
    CollectionReference exercises =
        FirebaseFirestore.instance.collection('history');

    var requestedExercises = await exercises
        .where("owner", isEqualTo: await firebaseAuth.getUserId())
        .get();

    return requestedExercises.docs.map((e) => _docToHistory(e)).toList();
  }

  @override
  Future<List<ExersiceHistory>> getExersiceHistory(
      ExerciseData exercise) async {
    CollectionReference exercises =
        FirebaseFirestore.instance.collection('history');

    var requestedExercises = await exercises
        .where("owner", isEqualTo: await firebaseAuth.getUserId())
        .where("exersiceId", isEqualTo: exercise.id)
        .get();

    return requestedExercises.docs.map((e) => _docToHistory(e)).toList();
  }

  @override
  Future<void> saveHistoy(
      ExersiceHistory exersiceHistory, ExerciseData exercise) async {
    CollectionReference historys =
        FirebaseFirestore.instance.collection('history');

    String userId = await firebaseAuth.getUserId();
    String exerciseId = exercise.id;

    return historys.add(_historyToMap(exersiceHistory, userId, exerciseId));
  }
}
