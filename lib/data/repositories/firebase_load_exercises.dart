import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:aiTrainer/data/datasources/firebase_auth_inteface.dart';
import 'package:aiTrainer/data/models/dynamic_exercise_firebase.dart';
import 'package:aiTrainer/domain/entities/exercise_data.dart';
import 'package:aiTrainer/domain/entities/dynamic_exercise.dart';
import 'package:aiTrainer/domain/repositories/load_exercises.dart';

class FirebaseLoadExercises implements LoadExercises {
  DocumentSnapshot lastDocumentSnapshot = null;
  bool searching = false;
  bool noMoreExercies = false;

  static int lim = 3;

  FirebaseAuthInteface firebaseAuth;

  FirebaseLoadExercises({
    @required this.firebaseAuth,
  });

  @override
  Future<DynamicExercise> getExerciseValue(ExerciseData exerciseData) {
    return Future.value(exerciseData as DynamicExercise);
  }

  Future<List<ExerciseData>> getExercises(
      bool userExercises, String name) async {
    if (searching) return List<ExerciseData>.empty();
    if (noMoreExercies) return List<ExerciseData>.empty();

    searching = true;
    CollectionReference exercises =
        FirebaseFirestore.instance.collection('exercise');

    Query getQuery = exercises;

    String userId = await firebaseAuth.getUserId();

    if (userExercises) {
      getQuery = exercises.where("owner", isEqualTo: userId);
    } else {
      getQuery = exercises;
    }

    if (name != null && name != "") {
      getQuery = getQuery
          .orderBy("indexName")
          .startAt([name]).endAt([name + "\uf8ff"]);
    }

    getQuery = getQuery.limit(lim);

    if (lastDocumentSnapshot != null) {
      getQuery = getQuery.startAfterDocument(lastDocumentSnapshot);
    }

    var requestedExercises = await getQuery.get();
    if (requestedExercises.docs.isEmpty ||
        requestedExercises.docs.length < lim) {
      noMoreExercies = true;
    } else {
      lastDocumentSnapshot = requestedExercises.docs.last;
    }
    searching = false;
    return requestedExercises.docs
        .map((e) => DynamicExerciseFirebase.fromMap(e, e.id))
        .toList();
  }

  @override
  Future<List<ExerciseData>> getRestExercises(String name) {
    return getExercises(false, name);
  }

  @override
  Future<List<ExerciseData>> getUserExercises(String name) {
    return getExercises(true, name);
  }

  @override
  void reset() {
    lastDocumentSnapshot = null;
    searching = false;
    noMoreExercies = false;
  }
}
