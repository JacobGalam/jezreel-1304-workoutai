import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:aiTrainer/data/datasources/firebase_auth_inteface.dart';
import 'package:aiTrainer/data/models/dynamic_exercise_firebase.dart';
import 'package:aiTrainer/domain/entities/dynamic_exercise.dart';
import 'package:aiTrainer/domain/repositories/credentials_repostory.dart';
import 'package:aiTrainer/domain/repositories/save_exercises.dart';

class FirebaseSaveExercises implements SaveExercises {
  CredentialsRepostory firebaseAuth;

  FirebaseSaveExercises({
    @required this.firebaseAuth,
  });

  @override
  Future<void> saveExercise(DynamicExercise exersice) async {
    CollectionReference exercises =
        FirebaseFirestore.instance.collection('exercise');

    exersice.owner = await firebaseAuth.getUserId();

    File file = File(exersice.imagePath);

    String randomName = DateTime.now().millisecondsSinceEpoch.toString();

    Reference storageReference =
        FirebaseStorage.instance.ref('uploads/$randomName');

    final UploadTask uploadTask = storageReference.putFile(file);

    exersice.imagePath = await (await uploadTask).ref.getDownloadURL();

    DynamicExerciseFirebase uploadExercies =
        DynamicExerciseFirebase.fromData(exersice);

    Map<String, dynamic> uploadExerciesMap = uploadExercies.toMap();

    return exercises.add(uploadExerciesMap);
  }
}
