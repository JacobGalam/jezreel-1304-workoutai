import 'dart:async';

import 'package:aiTrainer/data/datasources/posenet_souce.dart';
import 'package:aiTrainer/domain/entities/exercise_value.dart';
import 'package:aiTrainer/domain/entities/on_new_image_data.dart';
import 'package:aiTrainer/domain/entities/processed_count_exercise.dart';
import 'package:aiTrainer/domain/entities/processed_time_exercise.dart';
import 'package:aiTrainer/domain/entities/recognizing.dart';
import 'package:aiTrainer/domain/entities/video_metadata.dart';
import 'dart:io';

import 'dart:math';

import 'package:aiTrainer/domain/repositories/video_ai_read.dart';
import 'package:tuple/tuple.dart';

class VideoAiReadImp extends VideoAiRead {
  var values = new List<double>();
  var reps = 0;
  PosenetSource _posenetSource;

  VideoAiReadImp(this._posenetSource);

  Future<File> readVideo(VideoMetadata metadata, Stream<File> images,
      int numberOfImages, String bodyPart, onfind) async {
    // TODO: implement readVideo
    int i = 0;
    File firstFrame;
    await for (File image in images) {
      if (firstFrame == null) {
        firstFrame = image;
      }
      int height = metadata.videoHeight;
      int width = metadata.videoWidth;
      // TODO: use dataSource
      var posenetRegeneration =
          await this._posenetSource.recognizeFile(image.path);

      Recognizing recognizing = posenetRegeneration;
      if (posenetRegeneration != null) {
        var angleValue = recognizing.getByName(bodyPart);

        values.add(angleValue);
      }
      i++;
      if (onfind != null) {
        onfind(OnNewImageData(
          image: image,
          present: i / numberOfImages,
          results: recognizing == null ? null : recognizing.keyPointsModel,
          videoHeight: height,
          videoWidth: width,
        ));
      }
    }

    // var exerciseValue = getExerciseValueCount(bodyPart);
    return firstFrame;
    // values.clear();
    // exerciseValue.item1.part = bodyPart;
    // return ProcessedCountExercise(
    //   exerciseValue: exerciseValue.item1,
    //   tomnail: firstFrame,
    //   reps: exerciseValue.item2,
    // );
  }

  Tuple2<ExerciseValue, int> getExerciseValueCount(String bodyPart) {
    ExerciseValue bestValue = null;
    int bestReps = -1;
    int partTake = 10;
    int i = 2;
    bool notFound = true;
    while (notFound) {
      if (i == this.values.length) {
        i = 2;
        partTake--;
      }
      if (partTake == 0) {
        break;
      }
      var partOfRep = this.values.sublist(0, i);
      ExerciseValue exrciesValue =
          tryToGetExrciesValue(partOfRep, partTake, bodyPart);
      int nowReps = checkRest(this.values, exrciesValue);
      bool fail = nowReps != this.reps;
      if (fail) {
        if ((nowReps - reps).abs() < (bestReps - reps).abs()) {
          bestReps = nowReps;
          bestValue = exrciesValue;
        }
      } else {
        notFound = false;
        return Tuple2(exrciesValue, nowReps);
      }

      i++;
    }
    return Tuple2(bestValue, bestReps);
  }

  ExerciseValue getExerciseValueTime(String bodyPart) {
    var maxValue = values.reduce(max);
    var minValue = values.reduce(min);
    return ExerciseValue(
      down: minValue,
      up: maxValue,
      part: bodyPart,
    );
  }

  ExerciseValue tryToGetExrciesValue(
      List<double> partOfRep, int partTake, String bodyPart) {
    var up = (takeFirst(partOfRep, partTake, true)).reduce(min);
    var down = (takeFirst(partOfRep, partTake, false)).reduce(max);
    ExerciseValue getExerciseValue =
        new ExerciseValue(up: up, down: down, part: bodyPart);
    return getExerciseValue;
  }

  int checkRest(List<double> values, ExerciseValue exrciesValue) {
    var up = exrciesValue.up;
    var down = exrciesValue.down;
    var _isDown = false;
    var _repetition = 0;
    for (var x in values) {
      if (x < down) {
        _isDown = true;
      }
      var isStanding = x > up;
      if (_isDown && isStanding) {
        _isDown = false;
        _repetition += 1;
      }
    }
    return _repetition;
  }

  List<double> takeFirst(List<double> partOfRep, int partTake, bool fromTop) {
    var present = partOfRep.length ~/ partTake;
    if (present == 0) {
      present = 1;
    }
    List<double> copyReps = []..addAll(partOfRep);
    copyReps.sort();
    if (fromTop) {
      copyReps = copyReps.reversed.toList();
    }
    var t = copyReps.sublist(0, present);
    return t;
  }

  @override
  Future<void> init() async {
    return await this._posenetSource.init();
  }

  @override
  Future<ProcessedCountExercise> readVideoCount(
    int reps,
    VideoMetadata metadata,
    Stream<File> images,
    int numberOfImages,
    String bodyPart,
    onNewImage,
  ) async {
    var thumbnail =
        await readVideo(metadata, images, numberOfImages, bodyPart, onNewImage);
    this.reps = reps;
    var exerciseValue = getExerciseValueCount(bodyPart);
    values.clear();
    exerciseValue.item1.part = bodyPart;
    return ProcessedCountExercise(
      exerciseValue: exerciseValue.item1,
      tomnail: thumbnail,
      countReps: exerciseValue.item2,
      trainReps: reps,
    );
  }

  @override
  Future<ProcessedTimeExercise> readVideoTime(
    VideoMetadata metadata,
    Stream<File> images,
    int numberOfImages,
    String bodyPart,
    onNewImage,
  ) async {
    var thumbnail =
        await readVideo(metadata, images, numberOfImages, bodyPart, onNewImage);
    var exerciseValue = getExerciseValueTime(bodyPart);
    values.clear();
    exerciseValue.part = bodyPart;
    return ProcessedTimeExercise(
      exerciseValue: exerciseValue,
      tomnail: thumbnail,
      totalTime: Duration(seconds: 69), // TODO: change to real duration
    ); //
  }
}
