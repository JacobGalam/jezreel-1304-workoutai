import 'dart:async';
import 'dart:io';

import 'package:aiTrainer/domain/entities/video_metadata.dart';
import 'package:aiTrainer/domain/entities/video_thumbnail.dart';
import 'package:aiTrainer/domain/repositories/video_metadata_reader.dart';
import 'package:media_info/media_info.dart';
import 'package:path_provider/path_provider.dart';

class VideoMetadataReaderImp implements VideoMetadataReader {
  @override
  Future<VideoMetadata> getMetadata(String path) async {
    var mediaInfo = MediaInfo();
    var videoInfo = await mediaInfo.getMediaInfo(path);

    var videoHeight = videoInfo['height'];
    var videoWidth = videoInfo['width'];
    var videoLength = Duration(milliseconds: videoInfo["durationMs"]);

    VideoMetadata metadata = new VideoMetadata(
      videoLength: videoLength,
      videoHeight: videoHeight,
      videoWidth: videoWidth,
    );

    return metadata;
  }

  @override
  Future<VideoThumbnail> getThumbnail(File images, String exerciseName) async {
    final directory = await getApplicationDocumentsDirectory();
    final thumbnailPath = '${directory.path}/$exerciseName';
    var firstFrame = images;
    firstFrame.copy(thumbnailPath);
    VideoThumbnail thumbnail = new VideoThumbnail(thumbnailPath);
    return thumbnail;
  }
}
