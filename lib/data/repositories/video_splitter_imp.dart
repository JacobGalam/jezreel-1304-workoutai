import 'dart:async';

import 'package:export_video_frame/export_video_frame.dart';
import 'package:aiTrainer/domain/entities/video_metadata.dart';
import 'dart:io';

import 'dart:math';

import 'package:aiTrainer/domain/repositories/video_splitter.dart';

class VideoSplitterImp implements VideoSplitter {
  Stream<Duration> _loopOverDurationsOfVideo(
      Duration videoLength, Duration interval) async* {
    for (var i = Duration.zero; i < videoLength; i += interval) {
      yield i;
    }
  }

  @override
  Stream<File> getImages(
      File video, Duration interval, VideoMetadata metadata) async* {
    var durations = _loopOverDurationsOfVideo(metadata.videoLength, interval);
    await for (var duration in durations) {
      yield await ExportVideoFrame.exportImageBySeconds(video, duration, 0);
    }
  }
}
