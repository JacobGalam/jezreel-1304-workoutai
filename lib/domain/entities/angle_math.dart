import 'dart:math';

import 'package:aiTrainer/domain/entities/key_points.dart';

// https://stackoverflow.com/a/1211243/13277578, find key 1
double calculateAngle(KeyPointModel k1, KeyPointModel k2, KeyPointModel k3) {
  double p12 = _calculateLengthSegment(k1, k2);
  double p13 = _calculateLengthSegment(k1, k3);
  double p23 = _calculateLengthSegment(k2, k3);
  double numerator = pow(p12, 2) + pow(p13, 2) - pow(p23, 2);
  double denominator = 2 * p12 * p13;
  double divided = numerator / denominator;
  return acos(divided) * 180 / pi;
}

double _calculateLengthSegment(KeyPointModel first, KeyPointModel secend) {
  double sumDiff =
      _diff(first.xPoint, secend.xPoint) + _diff(first.yPoint, secend.yPoint);
  return sqrt(sumDiff);
}

double _diff(double x, double y) {
  return pow((x - y), 2);
}
