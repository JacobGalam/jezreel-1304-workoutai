import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/exercise_data.dart';

import 'package:aiTrainer/domain/entities/exercise_value.dart';

class DynamicExercise extends ExerciseData {
  ExerciseValue exerciseValue;

  DynamicExercise({
    @required this.exerciseValue,
  }) {
    // this.type = getTypeName();
  }

  // String getTypeName();
}
