import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';

class ExerciseData {
  String name;
  String info;
  ExerciesType type;
  String imagePath;
  String owner;
  String id;

  ExerciseData({
    @required this.name,
    @required this.info,
    @required this.type,
    @required this.imagePath,
    @required this.owner,
    @required this.id,
  });
}
