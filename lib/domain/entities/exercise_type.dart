enum ExerciesType {
  count,
  time,
}

ExerciesType exerciesTypeFromString(String value) {
  return ExerciesType.values.firstWhere(
      (type) => type.toString().split(".").last == value,
      orElse: () => null);
}

String exerciesTypeToString(ExerciesType type) {
  return type.toString().split(".").last;
}
