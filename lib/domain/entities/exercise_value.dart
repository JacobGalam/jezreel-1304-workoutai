import 'package:flutter/foundation.dart';

class ExerciseValue {
  String part;
  double up;
  double down;

  ExerciseValue({
    @required this.part,
    @required this.up,
    @required this.down,
  });
}
