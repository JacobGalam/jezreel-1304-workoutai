import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';

import 'exersice_history.dart';

class ExersiceCountHistory extends ExersiceHistory {
  @override
  DateTime start;
  @override
  DateTime end;
  @override
  String exersiceName;

  @override
  ExerciesType type = ExerciesType.count;

  int reps;

  ExersiceCountHistory({
    @required this.start,
    @required this.end,
    @required this.exersiceName,
    @required this.reps,
  });

  @override
  String valueAsString() {
    return this.reps.toString();
  }
}
