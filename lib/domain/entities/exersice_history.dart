import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';

abstract class ExersiceHistory {
  DateTime start;
  DateTime end;
  String exersiceName;
  String owner;
  ExerciesType type;
  String exersiceId;

  ExersiceHistory({
    @required this.start,
    @required this.end,
    @required this.exersiceName,
    @required this.type,
    @required this.owner,
  });

  String valueAsString();
}
