import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/domain/entities/exersice_history.dart';

class ExersiceTimeHistory extends ExersiceHistory {
  @override
  DateTime start;
  @override
  DateTime end;
  @override
  String exersiceName;
  @override
  ExerciesType type = ExerciesType.time;

  Duration time;

  ExersiceTimeHistory({
    @required this.start,
    @required this.end,
    @required this.exersiceName,
    @required this.time,
  });

  @override
  String valueAsString() {
    return this.time.inMilliseconds.toString();
  }
}
