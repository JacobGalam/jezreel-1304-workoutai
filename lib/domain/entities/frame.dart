import 'package:camera/camera.dart';
import 'package:aiTrainer/domain/entities/rect.dart';

class Frame {
  CameraImage cameraFrame;
  RectImage size;
  Frame(this.cameraFrame, this.size);
}
