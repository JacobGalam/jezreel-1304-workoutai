class KeyPointModel {
  double xPoint;
  double yPoint;
  double confidence;
  KeyPointModel([this.xPoint, this.yPoint]);
}

class KeyPointsModel {
  KeyPointModel nose;
  KeyPointModel leftEye;
  KeyPointModel rightEye;
  KeyPointModel leftEar;
  KeyPointModel rightEar;
  KeyPointModel leftShoulder;
  KeyPointModel rightShoulder;
  KeyPointModel leftElbow;
  KeyPointModel rightElbow;
  KeyPointModel leftWrist;
  KeyPointModel rightWrist;
  KeyPointModel leftHip;
  KeyPointModel rightHip;
  KeyPointModel leftKnee;
  KeyPointModel rightKnee;
  KeyPointModel leftAnkle;
  KeyPointModel rightAnkle;
  List<dynamic> raw;
}
