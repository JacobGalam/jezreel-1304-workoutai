import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/key_points.dart';

class OnNewImageData {
  File image;
  double present;
  KeyPointsModel results;
  int videoHeight;
  int videoWidth;

  OnNewImageData({
    @required this.image,
    @required this.present,
    @required this.results,
    @required this.videoHeight,
    @required this.videoWidth,
  });
}
