import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/exercise_value.dart';
import 'package:aiTrainer/domain/entities/processed_exercise.dart';

class ProcessedCountExercise implements ProcessedExerciseAbs {
  int countReps;
  int trainReps;

  ProcessedCountExercise({
    @required this.exerciseValue,
    @required this.countReps,
    @required this.trainReps,
    @required this.tomnail,
  });

  @override
  ExerciseValue exerciseValue;

  @override
  File tomnail;

  @override
  bool failedToProcessed() {
    if (exerciseValue == null) {
      return true;
    }
    if (countReps == 0) {
      return true;
    }
    return false;
  }

  @override
  Future<double> getAccuracy() {
    if (countReps == 0) {
      return Future.value(0);
    }
    var diff = (countReps - trainReps).abs();
    if (diff > countReps) {
      return Future.value(countReps / diff);
    } else {
      var worngPresent = diff / countReps;
      return Future.value(1 - worngPresent);
    }
  }
}
