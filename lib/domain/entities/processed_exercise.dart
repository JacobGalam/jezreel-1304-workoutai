import 'dart:async';
import 'dart:io';

import 'package:aiTrainer/domain/entities/exercise_value.dart';

abstract class ProcessedExerciseAbs {
  ExerciseValue exerciseValue;
  File tomnail;

  ProcessedExerciseAbs(this.exerciseValue);

  bool failedToProcessed();

  Future<double> getAccuracy();
}
