import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/exercise_value.dart';
import 'package:aiTrainer/domain/entities/processed_exercise.dart';

class ProcessedTimeExercise implements ProcessedExerciseAbs {
  ProcessedTimeExercise({
    @required this.exerciseValue,
    @required this.tomnail,
    @required this.totalTime,
  });

  @override
  ExerciseValue exerciseValue;

  @override
  File tomnail;

  Duration totalTime;

  @override
  bool failedToProcessed() {
    if (exerciseValue == null) {
      return true;
    }
    return false;
  }

  @override
  Future<double> getAccuracy() {
    return Future.value(1);
  }
}
