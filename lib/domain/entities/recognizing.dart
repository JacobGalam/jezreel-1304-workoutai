import 'package:aiTrainer/domain/entities/key_points.dart';
import 'package:aiTrainer/domain/entities/rect.dart';

class Recognizing {
  KeyPointsModel keyPointsModel = new KeyPointsModel();

  static List<String> names = [
    "Neck",
    "Right Shoulder",
    "Left Shoulder",
    "Right Chest",
    "Left Chest",
    "Right Elbow",
    "Left Elbow",
    "Right Hip",
    "Left Hip",
    "Right Knee",
    "Left Knee",
  ];

  bool found = false;
  double neck;
  double rightShoulder;
  double leftShoulder;
  double rightChest; // check if needed
  double leftChest;
  double rightElbow;
  double leftElbow;
  double rightHip; // check if needed more
  double leftHip;
  double rightKnee;
  double leftKnee;

  RectImage imageSize;

  static List<String> getNames() {
    return names;
  }

  double getByName(String name) {
    var index = names.indexOf(name);
    if (index == -1) {
      throw new Exception("Can't find name");
    }
    switch (index) {
      case 0:
        return this.neck;
      case 1:
        return this.rightShoulder;
      case 2:
        return this.leftShoulder;
      case 3:
        return this.rightChest;
      case 4:
        return this.leftChest;
      case 5:
        return rightElbow;
      case 6:
        return leftElbow;
      case 7:
        return rightHip;
      case 8:
        return leftHip;
      case 9:
        return rightKnee;
      case 10:
        return leftKnee;
    }
  }
}
