import 'package:flutter/foundation.dart';

class VideoMetadata {
  Duration videoLength;
  int videoHeight;
  int videoWidth;

  VideoMetadata({
    @required this.videoLength,
    @required this.videoHeight,
    @required this.videoWidth,
  });
}
