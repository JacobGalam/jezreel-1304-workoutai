import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';

class VideoUploadData {
  String name;
  String anglePart;
  String videoPath;
  String info;
  ExerciesType type;
  int reps;

  VideoUploadData({
    @required this.name,
    @required this.anglePart,
    @required this.videoPath,
    @required this.info,
    @required this.reps,
    @required this.type,
  });

  bool isTime() {
    return this.type == ExerciesType.time;
  }

  bool isCount() {
    return this.type == ExerciesType.count;
  }
}
