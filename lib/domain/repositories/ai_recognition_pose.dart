import 'dart:async';

import 'package:aiTrainer/domain/entities/frame.dart';
import 'package:aiTrainer/domain/entities/recognizing.dart';

abstract class AiRecognitionPose {
  Future<void> init();

  Future<Recognizing> recognize(Frame frame);
}
