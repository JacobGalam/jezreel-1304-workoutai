import 'dart:async';

import 'package:camera/camera.dart';
import 'package:aiTrainer/domain/entities/frame.dart';
import 'package:aiTrainer/domain/entities/rect.dart';

abstract class CameraManager {
  Future<void> init(Function(Frame) callback, RectImage wantedSize);
  Future<void> stopFrameStreaming();
  Future<CameraController> getController();
  bool haveAlsoBackCamera();
  Future<void> switchCamera();
}
