import 'dart:async';

import 'package:aiTrainer/domain/entities/login_data.dart';

abstract class CredentialsRepostory {
  Future<LoginData> login();

  Future<void> logout();

  Future<LoginData> getLoagedData();

  Future<String> getUserId();
}
