import 'dart:async';

import 'package:aiTrainer/domain/entities/exercise_data.dart';
import 'package:aiTrainer/domain/entities/exersice_history.dart';

abstract class History {
  Future<List<ExersiceHistory>> getExersiceHistory(ExerciseData exercise);
  Future<List<ExersiceHistory>> getAllExersiceHistory();
  Future<void> saveHistoy(
      ExersiceHistory exersiceHistory, ExerciseData exercise);
}
