import 'dart:async';

import 'package:aiTrainer/domain/entities/dynamic_exercise.dart';
import 'package:aiTrainer/domain/entities/exercise_data.dart';

abstract class LoadExercises {
  void reset();
  Future<List<ExerciseData>> getRestExercises(String name);
  Future<List<ExerciseData>> getUserExercises(String name);
  Future<DynamicExercise> getExerciseValue(ExerciseData exerciseData);
}
