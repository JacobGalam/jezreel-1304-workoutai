import 'dart:async';
import 'package:aiTrainer/domain/entities/dynamic_exercise.dart';

abstract class SaveExercises {
  Future<void> saveExercise(DynamicExercise exersice);
}
