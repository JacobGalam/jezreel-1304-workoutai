import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/dynamic_exercise.dart';
import 'package:aiTrainer/domain/entities/exersice_count_history.dart';
import 'package:aiTrainer/domain/entities/exersice_history.dart';
import 'package:aiTrainer/domain/entities/recognizing.dart';
import 'package:aiTrainer/domain/repositories/train_exercise.dart';

class TrainCountExercise implements TrainExercise {
  int reps = 0;
  int frames = 0;
  bool _isDown = false;
  @override
  Function(String state) onNewState;

  DynamicExercise dynamicCountExercise;

  TrainCountExercise({
    @required this.dynamicCountExercise,
    @required this.onNewState,
  });

  void addRecognizing(Recognizing recognizing) {
    double checkAngle = -1;
    if (recognizing != null) {
      checkAngle =
          recognizing.getByName(dynamicCountExercise.exerciseValue.part);

      if (checkAngle < dynamicCountExercise.exerciseValue.down) {
        _isDown = true;
        print("DOWN");
      }

      final isStanding = checkAngle > dynamicCountExercise.exerciseValue.up;
      if (_isDown && isStanding) {
        _isDown = false;
        print("UP");
        reps++;
      }
    }
    frames++;
    //print("$checkAngle, $_isDown, $frames");
    onNewState(this.reps.toString());
  }

  @override
  String getState() {
    return reps.toString();
  }

  @override
  ExersiceHistory saveHistory(DateTime start, String exersiceName) {
    return new ExersiceCountHistory(
        start: start,
        end: DateTime.now(),
        reps: reps,
        exersiceName: exersiceName);
  }

  @override
  void start() {}

  @override
  void stop() {
    reps = 0;
  }

  @override
  String getComperableState() {
    return this.getState();
  }
}
