import 'package:aiTrainer/domain/entities/dynamic_exercise.dart';
import 'package:aiTrainer/domain/entities/exersice_history.dart';
import 'package:aiTrainer/domain/entities/recognizing.dart';

abstract class TrainExercise {
  Function(String) onNewState;

  void start();
  TrainExercise(DynamicExercise trainData);
  void addRecognizing(Recognizing recognizing);
  void stop();
  String getState();
  String getComperableState();
  ExersiceHistory saveHistory(DateTime start, String exersiceName);
}
