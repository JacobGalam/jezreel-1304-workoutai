import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/dynamic_exercise.dart';
import 'package:aiTrainer/domain/entities/exersice_history.dart';
import 'package:aiTrainer/domain/entities/exersice_time_history.dart';
import 'package:aiTrainer/domain/entities/recognizing.dart';
import 'package:aiTrainer/domain/repositories/train_exercise.dart';

// start counting when in pose for more than one secend
// stop counting when not in pose for more than one secend
// with callbacks: stop being in pose. start being in pose for how muchg time
// and return the history for how many time was in the pose

class TrainTimeExercise implements TrainExercise {
  static int millisecondsToReset = 1000;

  @override
  Function(String state) onNewState;

  DateTime startDownTime;
  DateTime startNotBeingInPose;
  bool isDown = false;
  Timer _timer;
  String _stateAsString;

  DynamicExercise dynamicCountExercise;

  TrainTimeExercise({
    @required this.dynamicCountExercise,
    @required this.onNewState,
  });

  void _updateState() {
    var newState = getState();
    if (newState == _stateAsString) {
      return;
    }
    _stateAsString = newState;
    onNewState(newState);
  }

  @override
  void addRecognizing(Recognizing recognizing) {
    double checkAngle = -1;
    if (recognizing != null) {
      checkAngle =
          recognizing.getByName(dynamicCountExercise.exerciseValue.part);
      var upValue = dynamicCountExercise.exerciseValue.up;
      var downValue = dynamicCountExercise.exerciseValue.down;
      var isInPose = checkAngle <= upValue && checkAngle >= downValue;
      print(isInPose);
      if (!isInPose) {
        if (startNotBeingInPose == null) {
          startNotBeingInPose = DateTime.now();
        }
        if (isNotInPoseForLong()) {
          isDown = false;
        }
      } else {
        startNotBeingInPose = null;
        if (startDownTime == null) {
          startDownTime = DateTime.now();
        }
        if (inPoseForLong()) {
          var newStateInPost = isDown == false;
          if (newStateInPost) {
            startDownTime = DateTime.now();
            var timeUntilRecognizeIsInPost =
                Duration(milliseconds: millisecondsToReset);
            startDownTime.add(timeUntilRecognizeIsInPost);
          }
          isDown = true;
        }
      }
    }

    //print("$checkAngle, $isDown");
  }

  Duration getPoseDuration() {
    if (startDownTime == null) {
      return Duration.zero;
    }

    var d = DateTime.now().difference(startDownTime);
    //print("D:" + d.inMilliseconds.toString());
    return d;
  }

  bool inPoseForLong() {
    var poseDuration = getPoseDuration();
    return poseDuration.inMilliseconds > millisecondsToReset;
  }

  bool isNotInPoseForLong() {
    var notInPoseDuration = DateTime.now().difference(startNotBeingInPose);
    return notInPoseDuration.inMilliseconds > millisecondsToReset;
  }

  @override
  String getState() {
    var duration = getPoseDuration();
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    String milliseconds = twoDigits(duration.inMilliseconds.remainder(1000));

    return "$twoDigitMinutes:$twoDigitSeconds:$milliseconds";
  }

  @override
  ExersiceHistory saveHistory(DateTime start, String exersiceName) {
    var nowTime = DateTime.now();
    var totalTime = getPoseDuration();
    return new ExersiceTimeHistory(
        end: nowTime,
        start: start,
        exersiceName: exersiceName,
        time: totalTime);
  }

  @override
  void start() {
    const oneMili = const Duration(milliseconds: 1);
    _timer = new Timer.periodic(oneMili, (Timer t) => _updateState());
  }

  @override
  void stop() {
    _timer.cancel();
    startDownTime = null;
    startNotBeingInPose = null;
    isDown = false;
  }

  @override
  String getComperableState() {
    return getPoseDuration().inSeconds.toString();
  }
}
