import 'dart:async';
import 'dart:io';

import 'package:aiTrainer/domain/entities/processed_count_exercise.dart';
import 'package:aiTrainer/domain/entities/on_new_image_data.dart';
import 'package:aiTrainer/domain/entities/processed_time_exercise.dart';
import 'package:aiTrainer/domain/entities/video_metadata.dart';

typedef void OnFound(OnNewImageData data);

abstract class VideoAiRead {
  Future<void> init();
  Future<ProcessedCountExercise> readVideoCount(
    int reps,
    VideoMetadata metadata,
    Stream<File> images,
    int numberOfImages,
    String bodyPart,
    OnFound onNewImage,
  );
  Future<ProcessedTimeExercise> readVideoTime(
    VideoMetadata metadata,
    Stream<File> images,
    int numberOfImages,
    String bodyPart,
    OnFound onNewImage,
  );
}
