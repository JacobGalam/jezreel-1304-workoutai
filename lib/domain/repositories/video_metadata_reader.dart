import 'dart:async';
import 'dart:io';

import 'package:aiTrainer/domain/entities/video_metadata.dart';
import 'package:aiTrainer/domain/entities/video_thumbnail.dart';

abstract class VideoMetadataReader {
  Future<VideoThumbnail> getThumbnail(File images, String exerciseName);
  Future<VideoMetadata> getMetadata(String path);
}
