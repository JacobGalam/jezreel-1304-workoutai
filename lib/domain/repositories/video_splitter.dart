import 'dart:async';
import 'dart:io';

import 'package:aiTrainer/domain/entities/video_metadata.dart';

abstract class VideoSplitter {
  Stream<File> getImages(File video, Duration interval, VideoMetadata metadata);
}
