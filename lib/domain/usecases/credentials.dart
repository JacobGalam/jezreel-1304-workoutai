import 'dart:async';

import 'package:aiTrainer/domain/entities/login_data.dart';
import 'package:aiTrainer/domain/repositories/credentials_repostory.dart';

class Credentials {
  CredentialsRepostory loginRepo;

  Credentials({this.loginRepo});

  Future<LoginData> login() {
    return loginRepo.login();
  }

  Future<void> logout() {
    return loginRepo.logout();
  }

  Future<LoginData> getLoagedData() {
    return loginRepo.getLoagedData();
  }
}
