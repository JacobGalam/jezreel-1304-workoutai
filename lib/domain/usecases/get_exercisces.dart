import 'dart:async';

import 'package:aiTrainer/domain/entities/exercise_data.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/domain/repositories/load_exercises.dart';
import 'package:aiTrainer/domain/repositories/train_count_exercise.dart';
import 'package:aiTrainer/domain/repositories/train_exercise.dart';
import 'package:aiTrainer/domain/repositories/train_time_exercise.dart';
import 'package:tuple/tuple.dart';

class GetExercisces {
  LoadExercises exercisesImp;
  String lastSearch = "";
  GetExercisces(this.exercisesImp);

  // return if need to reset the list and the new adding/overide list
  Future<Tuple2<List<ExerciseData>, bool>> getExercisces(
      bool userExercises, String nameSearch, bool start) async {
    List<ExerciseData> exercise;
    bool needToRest = lastSearch != nameSearch;
    lastSearch = nameSearch;

    if (needToRest && !start) {
      reset();
      var waitForMoreInput = Future.delayed(Duration(
        milliseconds: 250,
      ));
      await waitForMoreInput;

      bool hasBeenChanged = lastSearch != nameSearch;
      if (hasBeenChanged) {
        return Tuple2(List<ExerciseData>.empty(), false);
      }
    }

    if (userExercises) {
      exercise = await exercisesImp.getUserExercises(nameSearch);
    } else {
      exercise = await exercisesImp.getRestExercises(nameSearch);
    }
    return Tuple2(exercise, needToRest);
  }

  void reset() {
    exercisesImp.reset();
  }

  Future<TrainExercise> getExercise(
      ExerciseData exerciseData, Function(String) _newStateCallback) async {
    var exerciseValue = await exercisesImp.getExerciseValue(exerciseData);
    if (exerciseData.type == ExerciesType.time)
      return TrainTimeExercise(
        dynamicCountExercise: exerciseValue,
        onNewState: _newStateCallback,
      );
    else if (exerciseData.type == ExerciesType.count) {
      return TrainCountExercise(
        dynamicCountExercise: exerciseValue,
        onNewState: _newStateCallback,
      );
    } else {
      throw new Exception("unknown type");
    }
  }
}
