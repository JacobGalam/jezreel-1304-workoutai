import 'dart:async';

import 'package:aiTrainer/domain/entities/exercise_data.dart';
import 'package:aiTrainer/domain/entities/exersice_history.dart';
import 'package:aiTrainer/domain/repositories/history.dart';

class GetHistory {
  History localDatabase;

  GetHistory(this.localDatabase);

  Future<List<ExersiceHistory>> getHistory(ExerciseData exercise) {
    return localDatabase.getExersiceHistory(exercise);
  }

  Future<List<ExersiceHistory>> getAllExersiceHistory() {
    return localDatabase.getAllExersiceHistory();
  }
}
