import 'dart:async';

import 'package:camera/camera.dart';
import 'package:aiTrainer/domain/repositories/camara_manager.dart';

class SetAndGetCamera {
  CameraManager _cameraManager;

  SetAndGetCamera(this._cameraManager);

  Future<CameraController> switchCamera() async {
    if (_cameraManager.haveAlsoBackCamera()) {
      await _cameraManager.stopFrameStreaming();
      await _cameraManager.switchCamera();
    }
    return _cameraManager.getController();
  }

  bool haveTwoCameras() {
    return _cameraManager.haveAlsoBackCamera();
  }

  Future<CameraController> getCameraControler() async {
    return _cameraManager.getController();
  }
}
