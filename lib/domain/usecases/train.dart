import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/exercise_data.dart';
import 'package:aiTrainer/domain/entities/frame.dart';
import 'package:aiTrainer/domain/entities/key_points.dart';
import 'package:aiTrainer/domain/entities/recognizing.dart';
import 'package:aiTrainer/domain/repositories/ai_recognition_pose.dart';
import 'package:aiTrainer/domain/repositories/camara_manager.dart';
import 'package:aiTrainer/domain/repositories/history.dart';
import 'package:aiTrainer/domain/repositories/train_exercise.dart';
import 'package:aiTrainer/domain/usecases/get_exercisces.dart';

class Train {
  final CameraManager cameraManager;
  final GetExercisces loadExercises;
  final AiRecognitionPose aiRecognitionPose;
  final History localDatabase;

  Function(String) _newStateCallback;
  Function(KeyPointsModel skeleton, int imageHeight, int imageWidth) _onPose;
  bool _inited = false;

  TrainExercise _trainExercise;
  DateTime _start;
  ExerciseData _exercise;

  Train({
    @required this.cameraManager,
    @required this.loadExercises,
    @required this.aiRecognitionPose,
    @required this.localDatabase,
  });

  Future<void> stop() async {
    //_trainExercise.clearState();
    return cameraManager.stopFrameStreaming();
  }

  Future<void> choseExercisce(ExerciseData exercise) async {
    if (!_inited) {
      await _init();
    }
    _exercise = exercise;
    _trainExercise =
        await loadExercises.getExercise(exercise, this._newStateCallback);

    _trainExercise.start();
    _start = DateTime.now();
  }

  void setStateCallback(
    Function(String) newStateCallback,
    Function(KeyPointsModel skeleton, int imageHeight, int imageWidth) onPose,
  ) {
    _newStateCallback = newStateCallback;
    if (_trainExercise != null) {
      _trainExercise.onNewState = _newStateCallback;
    }
    _newStateCallback(null);
    this._onPose = onPose;
  }

  Future<void> saveHistory() {
    var history = _trainExercise.saveHistory(_start, _exercise.name);

    _trainExercise.stop();
    return localDatabase.saveHistoy(history, _exercise);
  }

  Future<void> _init() async {
    return await Future.wait([
      cameraManager.init(_onFrame, null),
      aiRecognitionPose.init(),
    ]);
  }

  void _onFrame(Frame frame) async {
    Recognizing recognizing = await aiRecognitionPose.recognize(frame);

    this._trainExercise.addRecognizing(recognizing);

    // Update the gui
    _onPose(
      recognizing?.keyPointsModel,
      recognizing != null ? recognizing.imageSize.height.toInt() : 0,
      recognizing != null ? recognizing.imageSize.width.toInt() : 0,
    );
  }
}
