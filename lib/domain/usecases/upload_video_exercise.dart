import 'dart:async';
import 'dart:io';

import 'package:aiTrainer/domain/entities/dynamic_exercise.dart';
import 'package:aiTrainer/domain/entities/dynamic_time_exercise.dart';
import 'package:aiTrainer/domain/entities/key_points.dart';
import 'package:aiTrainer/domain/entities/processed_exercise.dart';
import 'package:aiTrainer/domain/entities/recognizing.dart';
import 'package:aiTrainer/domain/entities/video_upload_data.dart';
import 'package:aiTrainer/domain/repositories/save_exercises.dart';
import 'package:aiTrainer/domain/repositories/video_ai_read.dart';
import 'package:aiTrainer/domain/repositories/video_metadata_reader.dart';
import 'package:aiTrainer/domain/repositories/video_splitter.dart';
import 'package:aiTrainer/defs.dart' as defs;

const Duration interval = Duration(milliseconds: 400);

class UploadVideoExercise {
  VideoSplitter videoSplitter;
  VideoMetadataReader videoMetadataReader;
  VideoAiRead videoAiRead;
  SaveExercises saveExercise;

  UploadVideoExercise(
    this.videoSplitter,
    this.videoMetadataReader,
    this.videoAiRead,
    this.saveExercise,
  );

  Future<double> readVideo(
      VideoUploadData videoData, OnFound onNewImage) async {
    await videoAiRead.init();
    var metadata = await videoMetadataReader.getMetadata(videoData.videoPath);
    var videoFile = new File(videoData.videoPath);
    var imageStream = videoSplitter.getImages(videoFile, interval, metadata);
    int numberOfImages =
        metadata.videoLength.inMilliseconds ~/ interval.inMilliseconds;

    // TODO: update upload exercise
    ProcessedExerciseAbs processedExercise;
    if (videoData.isTime()) {
      processedExercise = await videoAiRead.readVideoTime(metadata, imageStream,
          numberOfImages, videoData.anglePart, onNewImage);
    } else if (videoData.isCount()) {
      processedExercise = await videoAiRead.readVideoCount(
          videoData.reps,
          metadata,
          imageStream,
          numberOfImages,
          videoData.anglePart,
          onNewImage);
    } else {
      throw new Exception("Can't hand this type of exercise");
    }
    var imagePath = videoMetadataReader.getThumbnail(
        processedExercise.tomnail, videoData.name);
    // var imagePath = videoMetadataReader.getThumbnail(
    //     videoData.videoPath, Duration.zero, videoData.name);
    var newExercise =
        new DynamicExercise(exerciseValue: processedExercise.exerciseValue);
    print("${newExercise.exerciseValue.up}, ${newExercise.exerciseValue.down}");
    newExercise.name = videoData.name;
    newExercise.info = videoData.info;
    newExercise.type = videoData.type; // TODO: change to time if need
    newExercise.imagePath = (await imagePath).path;
    if (processedExercise.failedToProcessed() == false) {
      await saveExercise.saveExercise(newExercise);
    }
    return processedExercise.getAccuracy();
  }

  Future<List<String>> getBodysParts() {
    return Future.value(Recognizing.getNames());
  }

  Future<List<String>> getTypesOfExercise() {
    return Future.value(
        [defs.timeExerciseTypeName, defs.countExerciseTypeName]);
  }
}
