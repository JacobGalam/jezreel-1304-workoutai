import 'dart:async';

import 'package:aiTrainer/data/datasources/camera_source.dart';
import 'package:aiTrainer/data/datasources/firebase_auth_inteface.dart';
import 'package:aiTrainer/data/datasources/posenet_souce.dart';
import 'package:aiTrainer/data/repositories/camara_manager_imp.dart';
import 'package:aiTrainer/data/repositories/firebase_credentials.dart';
import 'package:aiTrainer/data/repositories/firebase_history.dart';
import 'package:aiTrainer/data/repositories/firebase_load_exercises.dart';
import 'package:aiTrainer/data/repositories/firebase_save_exercises.dart';
import 'package:aiTrainer/data/repositories/video_ai_read_imp.dart';
import 'package:aiTrainer/data/repositories/video_metadata_reader_imp.dart';
import 'package:aiTrainer/domain/repositories/camara_manager.dart';
import 'package:aiTrainer/domain/repositories/credentials_repostory.dart';
import 'package:aiTrainer/domain/repositories/load_exercises.dart';
import 'package:aiTrainer/domain/repositories/history.dart';
import 'package:aiTrainer/domain/repositories/video_ai_read.dart';
import 'package:aiTrainer/domain/repositories/video_metadata_reader.dart';
import 'package:aiTrainer/domain/repositories/video_splitter.dart';
import 'package:aiTrainer/domain/usecases/get_exercisces.dart';
import 'package:aiTrainer/domain/usecases/get_history.dart';
import 'package:aiTrainer/domain/usecases/set_and_get_camara.dart';
import 'package:aiTrainer/domain/usecases/train.dart';
import 'package:aiTrainer/domain/usecases/upload_video_exercise.dart';
import 'package:aiTrainer/presentation/blocs/login_bloc/login_bloc.dart';
import 'package:aiTrainer/presentation/blocs/select_exercise_bloc/select_exercise_bloc.dart';
import 'package:aiTrainer/presentation/blocs/train_bloc/train_bloc.dart';
import 'package:aiTrainer/presentation/blocs/upload_video_bloc/upload_video_bloc.dart';
import 'package:get_it/get_it.dart';

import 'data/repositories/ai_recognition_pose_imp.dart';
import 'data/repositories/video_splitter_imp.dart';
import 'domain/repositories/ai_recognition_pose.dart';
import 'domain/repositories/save_exercises.dart';
import 'domain/usecases/credentials.dart';

final GetIt sl = GetIt.instance;

Future<void> init() async {
  // TODO: make named parameters for all the classes
  sl.registerFactory(
    () => TrainBloc(
      sl(),
      sl(),
      sl(),
      sl(),
    ),
  );

  sl.registerFactory(
    () => SelectExerciseBloc(
      sl(),
      sl(),
      sl(),
      sl(),
    ),
  );

  sl.registerFactory(
    () => UploadVideoBloc(sl()),
  );

  sl.registerLazySingleton(() => FirebaseAuthInteface());

  sl.registerFactory(() => LoginBloc(credentials: sl()));
  sl.registerFactory(() => Credentials(loginRepo: sl()));
  sl.registerFactory<CredentialsRepostory>(() => FirebaseCredentials(sl()));

  sl.registerFactory(
    () => UploadVideoExercise(sl(), sl(), sl(), sl()),
  );

  sl.registerFactory<VideoSplitter>(() => VideoSplitterImp());
  sl.registerFactory<VideoMetadataReader>(() => VideoMetadataReaderImp());
  sl.registerFactory<VideoAiRead>(() => VideoAiReadImp(sl()));
  sl.registerFactory<SaveExercises>(
      () => FirebaseSaveExercises(firebaseAuth: sl()));

  sl.registerFactory(() => GetHistory(sl()));
  sl.registerFactory(() => GetExercisces(sl()));

  // TODO: make singletons
  sl.registerLazySingleton<CameraManager>(() => CameraManagerImp(sl()));
  sl.registerLazySingleton<CameraSource>(() => CameraSourceImp());
  sl.registerFactory<PosenetSource>(() => PosenetSourceImp());
  sl.registerFactory<AiRecognitionPose>(() => AiRecognitionPoseImp(
        posenetSource: sl(),
      ));

  sl.registerLazySingleton(() => SetAndGetCamera(sl()));
  sl.registerLazySingleton(() => Train(
        aiRecognitionPose: sl(),
        loadExercises: sl(),
        cameraManager: sl(),
        localDatabase: sl(),
      ));
  sl.registerLazySingleton<History>(() => FirebaseHistory(firebaseAuth: sl()));

  sl.registerLazySingleton<LoadExercises>(() => FirebaseLoadExercises(
        firebaseAuth: sl(),
      ));
}
