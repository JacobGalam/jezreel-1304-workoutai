import 'dart:async';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:aiTrainer/presentation/blocs/select_exercise_bloc/select_exercise_bloc.dart';
import 'package:aiTrainer/presentation/blocs/train_bloc/train_bloc.dart';
import 'package:aiTrainer/presentation/blocs/upload_video_bloc/upload_video_bloc.dart';
import 'package:aiTrainer/presentation/pages/home_page.dart';
import 'injection_container.dart' as di;
// TODO: add unit testing for everything

// TARGET: call all the needed events and react to the states.
// TARGET: keep only the presentation logic, input and output logic.

Future<Null> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();

  WidgetsFlutterBinding.ensureInitialized();
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  final firebaseApp = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'AI Trainer',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue[800],
        accentColor: Colors.cyan[600],
      ),
      home: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (_) => di.sl<SelectExerciseBloc>(),
          ),
          BlocProvider(
            create: (_) => di.sl<UploadVideoBloc>(),
          ),
          BlocProvider(
            create: (_) => di.sl<TrainBloc>(),
          ),
        ],
        child: FutureBuilder(
          future: firebaseApp,
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              throw snapshot.error;
            } else if (snapshot.hasData) {
              return HomePage();
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }
}
