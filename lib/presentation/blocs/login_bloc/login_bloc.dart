import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:aiTrainer/domain/entities/login_data.dart';
import 'package:aiTrainer/domain/usecases/credentials.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  Credentials credentials;

  LoginBloc({this.credentials});

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is InitEvent) {
      yield LoadingState();
      LoginData usernameData = await credentials.getLoagedData();
      if (usernameData != null) {
        yield CanLoginState(usernameData);
      } else {
        yield LogoutState();
      }
    } else if (event is TryLoginEvent) {
      yield LoadingState();
      LoginData usernameData = await credentials.login();
      if (usernameData != null) {
        yield CanLoginState(usernameData);
      } else {
        yield LogoutState();
      }
    } else if (event is TryLogoutEvent) {
      yield LoadingState();
      await credentials.logout();
      yield LogoutState();
    }
  }

  @override
  LoginState get initialState {
    return LoadingState();
  }

  @override
  void onError(Object error, StackTrace stacktrace) {
    throw Exception(error);
  }
}
