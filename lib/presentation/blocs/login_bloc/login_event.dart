part of 'login_bloc.dart';

abstract class LoginEvent {
  const LoginEvent();
}

class TryLoginEvent extends LoginEvent {}

class InitEvent extends LoginEvent {}

class TryLogoutEvent extends LoginEvent {}
