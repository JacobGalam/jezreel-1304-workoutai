part of 'login_bloc.dart';

abstract class LoginState {
  const LoginState();
}

class CanLoginState extends LoginState {
  LoginData usernameData;

  CanLoginState(this.usernameData);
}

class LogoutState extends LoginState {}

class LoadingState extends LoginState {}
