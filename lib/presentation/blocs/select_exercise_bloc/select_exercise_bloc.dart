import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:aiTrainer/domain/entities/exercise_data.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/domain/entities/exersice_history.dart';
import 'package:aiTrainer/domain/entities/rect.dart';
import 'package:aiTrainer/domain/usecases/get_exercisces.dart';
import 'package:aiTrainer/domain/usecases/get_history.dart';
import 'package:aiTrainer/domain/usecases/set_and_get_camara.dart';
import 'package:aiTrainer/domain/usecases/train.dart';

part 'select_exercise_event.dart';
part 'select_exercise_state.dart';

class SelectExerciseBloc
    extends Bloc<SelectExerciseEvent, SelectExerciseState> {
  GetHistory saveAndGetHistory;
  GetExercisces getExercisces;
  Train train;
  SetAndGetCamera setAndGetCamera;

  SelectExerciseBloc(
    this.saveAndGetHistory,
    this.getExercisces,
    this.train,
    this.setAndGetCamera,
  ) : super();

  @override
  Stream<SelectExerciseState> mapEventToState(
    SelectExerciseEvent event,
  ) async* {
    if (event is GetAllHistoryEvent) {
      yield LoadingExerciseState();
      var allTimeHistoy = await saveAndGetHistory.getAllExersiceHistory();
      yield AllExersiceHistoryState(allTimeHistoy);
    } else if (event is GetExersiceHistoryEvent) {
      var history = await saveAndGetHistory.getHistory(event.exercise);
      yield GetExersiceHistoryState(history, event.exercise.type);
    } else if (event is GetMoreExersicesEvent) {
      if (event.firststart) {
        getExercisces.reset();
      }
      yield LoadingExerciseState();
      var exersices = await getExercisces.getExercisces(
          event.onlyUserExercises, event.searchName, event.firststart);
      if (exersices.item2) {
        yield GetExersicesOverideState(exersices.item1);
      } else {
        yield GetMoreExersicesState(exersices.item1);
      }
    } else if (event is MoveToTrainingEvent) {
      yield LoadingExerciseState();
      await train.choseExercisce(event.exercise);
      yield MovedToTrainingState(setAndGetCamera.haveTwoCameras(),
          await setAndGetCamera.getCameraControler(), event.exercise.type);
    }
  }

  @override
  void onError(Object error, StackTrace stacktrace) {
    throw Exception(error);
  }

  @override
  SelectExerciseState get initialState => Empty();
}
