part of 'select_exercise_bloc.dart';

abstract class SelectExerciseEvent {
  const SelectExerciseEvent();
}

class GetAllHistoryEvent extends SelectExerciseEvent {}

class GetExersiceHistoryEvent extends SelectExerciseEvent {
  ExerciseData exercise;
  GetExersiceHistoryEvent(this.exercise);
}

class GetMoreExersicesEvent extends SelectExerciseEvent {
  bool onlyUserExercises;
  bool firststart;
  String searchName;
  GetMoreExersicesEvent({
    @required this.onlyUserExercises,
    @required this.firststart,
    @required this.searchName,
  });
}

class MoveToTrainingEvent extends SelectExerciseEvent {
  RectImage wantSize;
  ExerciseData exercise;

  MoveToTrainingEvent(this.wantSize, this.exercise);
}
