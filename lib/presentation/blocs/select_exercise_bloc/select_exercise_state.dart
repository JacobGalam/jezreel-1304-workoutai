part of 'select_exercise_bloc.dart';

abstract class SelectExerciseState {
  const SelectExerciseState();
}

class Empty extends SelectExerciseState {}

class GetExersiceHistoryState extends SelectExerciseState {
  List<ExersiceHistory> exersiceHistory;
  ExerciesType type;
  GetExersiceHistoryState(this.exersiceHistory, this.type);
}

class AllExersiceHistoryState extends SelectExerciseState {
  List<ExersiceHistory> allExersiceHistory;
  AllExersiceHistoryState(this.allExersiceHistory);
}

class GetMoreExersicesState extends SelectExerciseState {
  List<ExerciseData> exersices;
  GetMoreExersicesState(this.exersices);
}

class GetExersicesOverideState extends SelectExerciseState {
  List<ExerciseData> exersices;
  GetExersicesOverideState(this.exersices);
}

class LoadingExerciseState extends SelectExerciseState {}

class MovedToTrainingState extends SelectExerciseState {
  bool haveTwoCameras;
  CameraController cameraController;
  ExerciesType type;

  MovedToTrainingState(this.haveTwoCameras, this.cameraController, this.type);
}
