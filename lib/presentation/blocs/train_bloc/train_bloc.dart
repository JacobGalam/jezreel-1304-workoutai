import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:camera/camera.dart';
import 'package:aiTrainer/domain/entities/key_points.dart';
import 'package:aiTrainer/domain/usecases/get_exercisces.dart';
import 'package:aiTrainer/domain/usecases/get_history.dart';
import 'package:aiTrainer/domain/usecases/set_and_get_camara.dart';
import 'package:aiTrainer/domain/usecases/train.dart';
import 'package:meta/meta.dart';

part 'train_event.dart';
part 'train_state.dart';

// TODO: make the events and states names betters
class TrainBloc extends Bloc<TrainEvent, TrainState> {
  SetAndGetCamera setAndGetCamera;
  Train train;
  GetHistory saveAndGetHistory;
  GetExercisces getExercisces;

  TrainBloc(
    this.setAndGetCamera,
    this.train,
    this.saveAndGetHistory,
    this.getExercisces,
  );

  @override
  Stream<TrainState> mapEventToState(
    TrainEvent event,
  ) async* {
    if (event is _NewTrainStateEvent) {
      yield NewTrainState(event.state);
    } else if (event is StartNewTraingEvent) {
      train.setStateCallback(onFind, onFindSkeleton);
    } else if (event is SwitchCamera) {
      var newCameraSouce = await setAndGetCamera.switchCamera();
      yield NewCameraState(newCameraSouce);
    } else if (event is ExitTrainingEvent) {
      yield LoadingExitState();
      await train.stop();
      await train.saveHistory();
      yield ExitState();
    } else if (event is _NewSkeletonEvent) {
      yield NewSkeletonState(
        event.skeleton != null ? event.skeleton.raw : null,
        event.imageHeight,
        event.imageWidth,
      );
    }
  }

  void onFind(String state) {
    this.add(_NewTrainStateEvent(state));
  }

  void onFindSkeleton(
      KeyPointsModel skeleton, int imageHeight, int imageWidth) {
    this.add(_NewSkeletonEvent(skeleton, imageHeight, imageWidth));
  }

  @override
  TrainState get initialState => Empty();

  @override
  void onError(Object error, StackTrace stacktrace) {
    throw Exception(error);
  }
}
