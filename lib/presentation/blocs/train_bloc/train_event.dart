part of 'train_bloc.dart';

@immutable
abstract class TrainEvent {}

class StartNewTraingEvent implements TrainEvent {}

class SwitchCamera implements TrainEvent {}

class ExitTrainingEvent implements TrainEvent {}

class _NewSkeletonEvent implements TrainEvent {
  KeyPointsModel skeleton;
  int imageHeight;
  int imageWidth;

  _NewSkeletonEvent(this.skeleton, this.imageHeight, this.imageWidth);
}

class _NewTrainStateEvent implements TrainEvent {
  String state;

  _NewTrainStateEvent(this.state);
}
