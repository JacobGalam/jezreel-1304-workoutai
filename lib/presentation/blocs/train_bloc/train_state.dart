part of 'train_bloc.dart';

@immutable
abstract class TrainState {}

class Empty extends TrainState {}

class LoadingNewCamera extends TrainState {}

class NewCameraState extends TrainState {
  CameraController cameraController; // TODO: dont use camera lib in the UI
  NewCameraState(this.cameraController);
}

class NewSkeletonState extends TrainState {
  List<dynamic> skeleton;
  int imageHeight;
  int imageWidth;

  NewSkeletonState(this.skeleton, this.imageHeight, this.imageWidth);
}

class NewTrainState extends TrainState {
  String value;

  NewTrainState(this.value);
}

class LoadingExitState extends TrainState {}

class ExitState extends TrainState {}
