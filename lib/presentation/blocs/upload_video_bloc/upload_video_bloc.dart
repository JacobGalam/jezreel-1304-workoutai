import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:aiTrainer/domain/entities/on_new_image_data.dart';
import 'package:aiTrainer/domain/entities/video_upload_data.dart';
import 'package:aiTrainer/domain/usecases/upload_video_exercise.dart';

part 'upload_video_event.dart';
part 'upload_video_state.dart';

class UploadVideoBloc extends Bloc<UploadVideoEvent, UploadVideoState> {
  // VideoPicker videoPicker;
  // UploadVideoBloc(this.videoPicker) : super();

  UploadVideoExercise videoExerciseRead;

  UploadVideoBloc(this.videoExerciseRead);

  void onNewPresentState(OnNewImageData data) {
    this.add(_PresentStateEvent(data));
  }

  @override
  Stream<UploadVideoState> mapEventToState(
    UploadVideoEvent event,
  ) async* {
    if (event is TryUploadVideoEvent) {
      yield StartUploadingVideoState();
      videoExerciseRead
          .readVideo(event.videoUploadData, onNewPresentState)
          .then((value) => this.add(_FinishUploadingVideoEvent(value)));
    } else if (event is GetUploadOptionsEvent) {
      var bodyParts = await videoExerciseRead.getBodysParts();
      var typesOfExercise = await videoExerciseRead.getTypesOfExercise();
      yield GetUploadOptionsState(bodyParts, typesOfExercise);
    } else if (event is _PresentStateEvent) {
      yield NewUploadPogressState(event.data);
    } else if (event is _FinishUploadingVideoEvent) {
      yield FinishUploadingVideoState(event.present);
    }
  }

  @override
  UploadVideoState get initialState => EmptyFormUploadState();

  @override
  void onError(Object error, StackTrace stacktrace) {
    throw Exception(error);
  }
}
