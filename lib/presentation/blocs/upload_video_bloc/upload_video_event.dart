part of 'upload_video_bloc.dart';

abstract class UploadVideoEvent {
  const UploadVideoEvent();
}

class TryUploadVideoEvent extends UploadVideoEvent {
  VideoUploadData videoUploadData;
  TryUploadVideoEvent(this.videoUploadData);
}

class GetUploadOptionsEvent extends UploadVideoEvent {}

class _PresentStateEvent extends UploadVideoEvent {
  OnNewImageData data;
  _PresentStateEvent(this.data);
}

class _FinishUploadingVideoEvent extends UploadVideoEvent {
  double present;
  _FinishUploadingVideoEvent(this.present);
}
