part of 'upload_video_bloc.dart';

abstract class UploadVideoState {
  const UploadVideoState();
}

class Empty extends UploadVideoState {}

class EmptyFormUploadState extends UploadVideoState {}

class StartUploadingVideoState extends UploadVideoState {}

class NewUploadPogressState extends UploadVideoState {
  OnNewImageData data;
  NewUploadPogressState(this.data);
}

class FinishUploadingVideoState extends UploadVideoState {
  double repDetectedPresent;
  FinishUploadingVideoState(this.repDetectedPresent);
}

class GetUploadOptionsState extends UploadVideoState {
  List<String> bodyParts;
  List<String> typesOfExercise;
  GetUploadOptionsState(this.bodyParts, this.typesOfExercise);
}
