import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:aiTrainer/domain/entities/exercise_data.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/domain/entities/exersice_count_history.dart';
import 'package:aiTrainer/domain/entities/exersice_history.dart';
import 'package:aiTrainer/domain/entities/exersice_time_history.dart';
import 'package:aiTrainer/domain/entities/rect.dart';
import 'package:aiTrainer/presentation/blocs/select_exercise_bloc/select_exercise_bloc.dart';
import 'package:aiTrainer/presentation/blocs/train_bloc/train_bloc.dart';
import 'package:aiTrainer/presentation/pages/train_page.dart';
import 'package:aiTrainer/presentation/widgets/history_widget.dart';

import '../../injection_container.dart' as di;

class ExerciseMoreInfoPage extends StatefulWidget {
  ExerciseData exerciseData;

  ExerciseMoreInfoPage(this.exerciseData);

  @override
  _ExerciseMoreInfoPageState createState() => _ExerciseMoreInfoPageState();
}

class _ExerciseMoreInfoPageState extends State<ExerciseMoreInfoPage> {
  ExersiceCountHistory _exersiceCountHistory;
  ExersiceTimeHistory _exersiceTimeHistory;

  void moveToVideoScreen(context, CameraController cameraController,
      bool doesHaveTwoCameras, bool isTimeExercise) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => BlocProvider.value(
          value: di.sl<TrainBloc>(),
          child:
              TrainPage(cameraController, doesHaveTwoCameras, isTimeExercise),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    var a = BlocProvider.of<SelectExerciseBloc>(context);
    a.add(GetExersiceHistoryEvent(this.widget.exerciseData));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SelectExerciseBloc, SelectExerciseState>(
        listener: (context, state) {
      if (state is MovedToTrainingState) {
        moveToVideoScreen(context, state.cameraController, state.haveTwoCameras,
            state.type == ExerciesType.time);
      } else if (state is GetExersiceHistoryState) {
        DateTime lowetStart = null;
        DateTime futurEnd = null;
        ExerciesType type = state.type;
        int totalValue = 0;

        for (var x in state.exersiceHistory) {
          if (lowetStart == null) {
            lowetStart = x.start;
          }
          if (lowetStart.isAfter(x.start)) {
            lowetStart = x.start;
          }
          if (futurEnd == null) {
            futurEnd = x.end;
          }
          if (!futurEnd.isAfter(x.end)) {
            futurEnd = x.end;
          }
          totalValue += int.parse(x.valueAsString());
        }
        setState(() {
          if (type == ExerciesType.time)
            this._exersiceTimeHistory = new ExersiceTimeHistory(
              end: futurEnd,
              start: lowetStart,
              time: Duration(milliseconds: totalValue),
              exersiceName: null,
            );
          else if (type == ExerciesType.count) {
            this._exersiceCountHistory = new ExersiceCountHistory(
              end: futurEnd,
              start: lowetStart,
              reps: totalValue,
              exersiceName: null,
            );
          } else {
            throw new Exception("unknown type");
          }
        });
      }
    }, child: BlocBuilder<SelectExerciseBloc, SelectExerciseState>(
      builder: (_, state) {
        return Container(
          child: Scaffold(
            body: DefaultTabController(
              length: 2,
              child: NestedScrollView(
                  headerSliverBuilder:
                      (BuildContext context, bool innerBoxIsScrolled) {
                    return [
                      SliverAppBar(
                        expandedHeight: 200.0,
                        floating: true,
                        pinned: true,
                        snap: true,
                        actionsIconTheme: IconThemeData(opacity: 0.0),
                        flexibleSpace: Stack(
                          children: <Widget>[
                            Positioned.fill(
                              child: Hero(
                                tag: widget.exerciseData.name,
                                child: Image.network(
                                  widget.exerciseData.imagePath,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ];
                  },
                  body: ListView(children: [
                    Center(
                      child: Text(
                        widget.exerciseData.name,
                        style: TextStyle(fontSize: 50),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                      child: Text(
                        widget.exerciseData.info,
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    _exersiceCountHistory != null
                        ? HistoryWidget(
                            _exersiceCountHistory.start,
                            _exersiceCountHistory.end,
                            _exersiceCountHistory.reps.toString(),
                            false)
                        : Container(),
                    _exersiceTimeHistory != null
                        ? HistoryWidget(
                            _exersiceTimeHistory.start,
                            _exersiceTimeHistory.end,
                            _exersiceTimeHistory.time.inSeconds.toString(),
                            true)
                        : Container(),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                        child: IconButton(
                      iconSize: 50,
                      icon: Icon(Icons.play_arrow),
                      color: Colors.green,
                      onPressed: () {
                        Size screen = MediaQuery.of(context).size;
                        RectImage rect = RectImage(screen.height, screen.width);
                        BlocProvider.of<SelectExerciseBloc>(context)
                          ..add(MoveToTrainingEvent(rect, widget.exerciseData));
                      },
                    )),
                  ])),
            ),
          ),
        );
      },
    ));
  }
}
