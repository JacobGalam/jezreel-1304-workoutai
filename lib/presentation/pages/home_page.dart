import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:aiTrainer/presentation/blocs/login_bloc/login_bloc.dart';
import 'package:aiTrainer/presentation/blocs/select_exercise_bloc/select_exercise_bloc.dart';
import 'package:aiTrainer/presentation/blocs/train_bloc/train_bloc.dart';
import 'package:aiTrainer/presentation/blocs/upload_video_bloc/upload_video_bloc.dart';
import 'package:aiTrainer/presentation/widgets/select_pages.dart';
import 'package:aiTrainer/presentation/pages/train_page.dart';
import 'package:aiTrainer/injection_container.dart' as di;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    updateExersices();
    // TODO: check if update history when finish train
    //BlocProvider.of<SelectExerciseBloc>(context)..add(GetAllHistoryEvent());
  }

  void updateExersices() {
    //BlocProvider.of<SelectExerciseBloc>(context)..add(GetAllExersicesEvent());
  }

  @override
  Widget build(BuildContext context) {
    return buildBody(context);
  }

  Widget buildBody(BuildContext context) {
    // TODO: fix to center
    return Scaffold(
        body: BlocListener<SelectExerciseBloc, SelectExerciseState>(
      listener: (context, state) {},
      child: BlocBuilder<TrainBloc, TrainState>(builder: (_, state) {
        return MultiBlocProvider(
          child: SelectPages(this.updateExersices),
          providers: [
            BlocProvider<SelectExerciseBloc>(
                create: (_) => BlocProvider.of<SelectExerciseBloc>(context)),
            BlocProvider<TrainBloc>(
                create: (_) => BlocProvider.of<TrainBloc>(context)),
            BlocProvider<UploadVideoBloc>(
                create: (_) => BlocProvider.of<UploadVideoBloc>(context)),
            BlocProvider<LoginBloc>(create: (_) => di.sl<LoginBloc>()),
          ],
        );
      }),
    ));
  }

  void moveToVideoScreen(context, CameraController cameraController,
      bool haveTwoCameras, bool isTimeExercise) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => BlocProvider.value(
          value: BlocProvider.of<TrainBloc>(context),
          child: TrainPage(cameraController, haveTwoCameras, isTimeExercise),
        ),
      ),
    );
  }
}
