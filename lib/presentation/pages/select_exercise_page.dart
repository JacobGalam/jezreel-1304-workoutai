import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:aiTrainer/domain/entities/exercise_data.dart';
import 'package:aiTrainer/presentation/blocs/select_exercise_bloc/select_exercise_bloc.dart';
import 'package:aiTrainer/presentation/blocs/upload_video_bloc/upload_video_bloc.dart';
import 'package:aiTrainer/presentation/widgets/exercise_item.dart';

int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 30;

class SelectExercisePage extends StatefulWidget {
  bool userExercises;

  SelectExercisePage(this.userExercises, {Key key}) : super(key: key);

  @override
  _SelectExercisePageState createState() => _SelectExercisePageState();
}

class _SelectExercisePageState extends State<SelectExercisePage> {
  List<ExerciseData> exercises = new List<ExerciseData>();
  bool waitingForData = true;
  String searchName = "";

  final scrollController = ScrollController();

  @override
  void didChangeDependencies() {
    BlocProvider.of<SelectExerciseBloc>(context)
      ..add(GetMoreExersicesEvent(
        searchName: searchName,
        firststart: true,
        onlyUserExercises: widget.userExercises,
      ));
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    scrollController.addListener(() {
      bool isScrooledDown = scrollController.offset >=
              scrollController.position.maxScrollExtent &&
          !scrollController.position.outOfRange;
      if (isScrooledDown) {
        BlocProvider.of<SelectExerciseBloc>(context)
          ..add(GetMoreExersicesEvent(
            searchName: searchName,
            firststart: false,
            onlyUserExercises: widget.userExercises,
          ));
      }
    });
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SelectExerciseBloc, SelectExerciseState>(
      listener: (context, state) {
        if (state is GetMoreExersicesState) {
          setState(() {
            this.exercises += state.exersices;
          });
        }
        if (state is GetExersicesOverideState) {
          this.exercises.clear();
          setState(() {
            this.exercises += state.exersices;
          });
        }
      },
      child: BlocBuilder<SelectExerciseBloc, SelectExerciseState>(
          builder: (_, state) {
        if (state is LoadingExerciseState) {
          waitingForData = true;
        } else {
          waitingForData = false;
        }
        return ListView.separated(
          separatorBuilder: (BuildContext context, int index) {
            return SizedBox(height: 10.0);
          },
          itemCount: exercises == null ? 1 : exercises.length + 2,
          itemBuilder: (BuildContext context, int index) {
            if (index == 0) {
              return Container(
                  child: Padding(
                padding: EdgeInsets.only(
                  right: 8,
                  left: 8,
                  top: 8,
                ),
                child: TextField(
                  onChanged: (String text) {
                    searchName = text;
                    BlocProvider.of<SelectExerciseBloc>(context)
                      ..add(GetMoreExersicesEvent(
                        searchName: text,
                        firststart: false,
                        onlyUserExercises: widget.userExercises,
                      ));
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    icon: Icon(Icons.search),
                    hintText: 'Enter a exercise name',
                  ),
                ),
              ));
            }
            if (index == exercises.length + 1) {
              if (waitingForData) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                return Container();
              }
            }
            index = index - 1;
            ExerciseData exercise = exercises[index];
            return MultiBlocProvider(
                child: ExerciseItem(
                  exerciseData: exercise,
                ),
                providers: [
                  BlocProvider<SelectExerciseBloc>.value(
                    value: BlocProvider.of<SelectExerciseBloc>(context),
                  ),
                  BlocProvider<UploadVideoBloc>.value(
                    value: BlocProvider.of<UploadVideoBloc>(context),
                  )
                ]);
          },
          controller: scrollController,
        );
      }),
    );
  }
}

class MyExercisePage extends SelectExercisePage {
  MyExercisePage() : super(true);
}

class ExercisePage extends SelectExercisePage {
  ExercisePage() : super(false);
}
