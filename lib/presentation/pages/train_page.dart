import 'package:camera/camera.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:aiTrainer/domain/entities/rect.dart';
import 'package:aiTrainer/presentation/blocs/train_bloc/train_bloc.dart';
import 'package:aiTrainer/presentation/blocs/select_exercise_bloc/select_exercise_bloc.dart';
import 'package:aiTrainer/presentation/blocs/upload_video_bloc/upload_video_bloc.dart';
import 'dart:math' as math;

import 'package:aiTrainer/presentation/pages/home_page.dart';
import 'package:aiTrainer/presentation/widgets/image_item.dart';
import 'package:aiTrainer/presentation/widgets/train_progress.dart';

import '../../injection_container.dart' as di;

class TrainPage extends StatefulWidget {
  CameraController cameraController;
  bool haveTwoCameras;
  bool isTimeExercise;

  TrainPage(this.cameraController, this.haveTwoCameras, this.isTimeExercise);

  @override
  _TrainPageState createState() => _TrainPageState();
}

class _TrainPageState extends State<TrainPage> {
  CameraController cameraController;
  List<dynamic> skeleton;
  int imageHeight;
  int imageWidth;

  @override
  void didChangeDependencies() {
    BlocProvider.of<TrainBloc>(context)..add(StartNewTraingEvent());
    cameraController = widget.cameraController;
    // _stopWatchTimer.onExecute.add(StopWatchExecute.start);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<TrainBloc, TrainState>(
        listener: (context, state) {
          if (state is NewTrainState) {
          } else if (state is ExitState) {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (_) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (_) => di.sl<SelectExerciseBloc>(),
                    ),
                    BlocProvider(
                      create: (_) => di.sl<UploadVideoBloc>(),
                    ),
                    BlocProvider(
                      create: (_) => di.sl<TrainBloc>(),
                    ),
                  ],
                  child: HomePage(),
                ),
              ),
            );
          } else if (state is LoadingExitState) {
          } else if (state is NewCameraState) {
            cameraController = state.cameraController;
          } else if (state is LoadingNewCamera) {
          } else if (state is NewSkeletonState) {
            skeleton = state.skeleton;
            imageHeight = state.imageHeight;
            imageWidth = state.imageWidth;
          }
        },
        child: BlocBuilder<TrainBloc, TrainState>(
          builder: (_, state) {
            var size = calculateStackSize();

            return Stack(children: [
              OverflowBox(
                maxHeight: size.height,
                maxWidth: size.width,
                child: CameraPreview(cameraController),
              ),
              skeleton != null
                  ? BndBox(skeleton, imageHeight, imageWidth, size.height,
                      size.width)
                  : Container(),
              Column(
                children: [
                  Row(children: [
                    new IconButton(
                        icon: Icon(
                          Icons.cancel,
                          color: Colors.red,
                        ),
                        onPressed: () {
                          BlocProvider.of<TrainBloc>(context)
                            ..add(ExitTrainingEvent());
                        }),
                    widget.haveTwoCameras
                        ? new IconButton(
                            icon: Icon(
                              Icons.switch_camera,
                              color: Colors.blue,
                            ),
                            onPressed: () {
                              BlocProvider.of<TrainBloc>(context)
                                ..add(SwitchCamera());
                            })
                        : Container(),
                  ]),
                  BlocProvider(
                      child: TrainProgress(),
                      create: (_) => BlocProvider.of<TrainBloc>(context)),
                ],
              )
            ]);
          },
        ),
      ),
    );
  }

  RectImage calculateStackSize() {
    var tmp = MediaQuery.of(context).size;
    var screenH = math.max(tmp.height, tmp.width);
    var screenW = math.min(tmp.height, tmp.width);
    tmp = widget.cameraController.value.previewSize;
    var previewH = math.max(tmp.height, tmp.width);
    var previewW = math.min(tmp.height, tmp.width);
    var screenRatio = screenH / screenW;
    var previewRatio = previewH / previewW;

    var maxHeight =
        screenRatio > previewRatio ? screenH : screenW / previewW * previewH;
    var maxWidth =
        screenRatio > previewRatio ? screenH / previewH * previewW : screenW;
    return RectImage(maxHeight, maxWidth);
  }
}
