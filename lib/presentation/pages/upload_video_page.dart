import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/domain/entities/on_new_image_data.dart';
import 'package:aiTrainer/domain/entities/video_upload_data.dart';
import 'package:aiTrainer/presentation/blocs/upload_video_bloc/upload_video_bloc.dart';
import 'package:aiTrainer/presentation/widgets/image_pose.dart';
import 'package:image_picker/image_picker.dart';
import 'package:aiTrainer/defs.dart' as defs;

class UploadVideoPage extends StatefulWidget {
  UploadVideoPage({Key key}) : super(key: key);

  @override
  _UploadVideoPageState createState() => _UploadVideoPageState();
}

class _TextField extends StatelessWidget {
  final Function(String) callback;
  final String name;
  final bool isNumber;

  _TextField({
    this.callback,
    this.name,
    this.isNumber = false,
  });

  @override
  Widget build(BuildContext context) {
    if (isNumber) {
      return TextField(
          onChanged: this.callback,
          decoration: new InputDecoration(labelText: name),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ]);
    } else {
      return TextField(
        onChanged: this.callback,
        decoration: new InputDecoration(labelText: name),
      );
    }
  }
}

class _UploadVideoPageState extends State<UploadVideoPage> {
  String _currentBodySelection = "";
  String _currentTypeSelection = "";
  String name;
  String info;
  String reps;
  String videoPath = "";
  List<String> exerciseTypes = [];
  List<String> bodyTypes = [];
  String _chosenValue;
  bool notSelectVideo = true;

  Widget showWidget = Container(child: Text("loading"));
  OnNewImageData imageData;

  double uploadProgress = 0;

  @override
  void initState() {
    super.initState();
    _getImage();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    BlocProvider.of<UploadVideoBloc>(context)..add(GetUploadOptionsEvent());
  }

  void _getImage() async {
    final picker = ImagePicker();
    var pickedFile = await picker.getVideo(source: ImageSource.gallery);

    setState(() {
      notSelectVideo = pickedFile == null;
      if (!notSelectVideo) this.videoPath = pickedFile.path;
    });
  }

  void _upload() {
    VideoUploadData videoUploadData = new VideoUploadData(
      anglePart: this._currentBodySelection,
      name: this.name,
      videoPath: this.videoPath,
      info: this.info,
      reps: this.reps != null ? int.parse(this.reps) : -1,
      type: exerciesTypeFromString(this._currentTypeSelection),
    );

    BlocProvider.of<UploadVideoBloc>(context)
      ..add(TryUploadVideoEvent(videoUploadData));
  }

  Widget buildImage(OnNewImageData data) {
    var size = MediaQuery.of(context).size;
    return Container(child: LayoutBuilder(builder: (_, constraints) {
      var i = ImagePose(
        Image.file(data.image),
        data.results != null ? data.results.raw : [],
        data.videoHeight + 270,
        data.videoWidth + 150,
        size.height,
        size.width,
      );
      return i;
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<UploadVideoBloc, UploadVideoState>(
        listener: (context, state) {
          if (state is GetUploadOptionsState) {
            setState(() {
              this.bodyTypes = state.bodyParts;
              this._currentBodySelection = this.bodyTypes.first;

              this.exerciseTypes = state.typesOfExercise;
              this._currentTypeSelection = this.exerciseTypes.first;
            });
          }
          if (state is NewUploadPogressState) {
            setState(() {
              uploadProgress = state.data.present;
              imageData = state.data;
            });
          }
        },
        child:
            BlocBuilder<UploadVideoBloc, UploadVideoState>(builder: (_, state) {
          if (notSelectVideo) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("Please select video"),
                  RaisedButton(
                    child: new Text('Select video'),
                    onPressed: _getImage,
                  ),
                ],
              ),
            );
          }

          if (state is EmptyFormUploadState) {
            return CircularProgressIndicator();
          } else if (state is GetUploadOptionsState) {
            var items = [
              Center(
                heightFactor: 2,
                child: Text(
                  "Upload Exercise",
                  style: TextStyle(fontSize: 40),
                ),
              ),
              _TextField(
                name: "Name of the exercise",
                callback: (newValue) => this.name = newValue,
              ),
              Container(
                padding: const EdgeInsets.all(0.0),
                child: DropdownButtonFormField(
                  value: _currentTypeSelection,
                  items: this.exerciseTypes.map((String value) {
                    return new DropdownMenuItem(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _currentTypeSelection = newValue;
                    });
                  },
                  decoration: InputDecoration(
                    filled: false,
                    labelText: "Exercise type",
                  ),
                ),
              ),
              Container(
                  padding: const EdgeInsets.all(0.0),
                  child: DropdownButtonFormField(
                    decoration: InputDecoration(
                      filled: false,
                      labelText: "Exercise angle",
                    ),
                    //Don't forget to pass your variable to the current value
                    value: _currentBodySelection,
                    items: this.bodyTypes.map((String value) {
                      return new DropdownMenuItem<String>(
                        value: value,
                        child: new Text(value),
                      );
                    }).toList(),
                    //On changed update the variable name and don't forgot the set state!
                    onChanged: (newValue) {
                      setState(() {
                        _currentBodySelection = newValue;
                      });
                    },
                  )),
              _currentTypeSelection == defs.countExerciseTypeName
                  ? _TextField(
                      name: "Number of repetitions in the video",
                      callback: (newValue) => this.reps = newValue,
                      isNumber: true,
                    )
                  : Container(),
              _TextField(
                name: "More info about the exercise",
                callback: (newValue) => this.info = newValue,
              ),
              Center(
                child: new RaisedButton(
                  child: new Text('Upload'),
                  onPressed: _upload,
                ),
              ),
            ];

            return ListView.separated(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              itemBuilder: (context, index) {
                return items[index];
              },
              itemCount: items.length,
              separatorBuilder: (context, index) {
                return SizedBox(
                  height: 10,
                );
              },
            );
          } else if (state is StartUploadingVideoState ||
              state is NewUploadPogressState ||
              state is Empty) {
            return Column(children: [
              Text("Loading... That can take few minutes"),
              LinearProgressIndicator(
                value: this.uploadProgress,
              ),
              Expanded(
                flex: 1,
                child: imageData == null ? Container() : buildImage(imageData),
              ),
            ]);
          } else if (state is FinishUploadingVideoState) {
            return Center(
                child: Text(
              "Finish! With accurate of ${state.repDetectedPresent * 100}%!",
            ));
          }
        }),
      ),
    );
  }
}
