import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/presentation/blocs/select_exercise_bloc/select_exercise_bloc.dart';
import 'package:aiTrainer/presentation/widgets/history_widget.dart';

class AllHistoryWidget extends StatefulWidget {
  AllHistoryWidget();

  @override
  _AllHistoryWidgetState createState() => _AllHistoryWidgetState();
}

class _AllHistoryWidgetState extends State<AllHistoryWidget> {
  //List<ExersiceHistory> exercisesHistory;
  DateTime lowetStart = null;
  DateTime futurEnd = null;
  Duration totalTime = null;
  int totalReps = null;

  @override
  void didChangeDependencies() {
    BlocProvider.of<SelectExerciseBloc>(context)..add(GetAllHistoryEvent());

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SelectExerciseBloc, SelectExerciseState>(
        listener: (context, state) {
      if (state is AllExersiceHistoryState) {
        int totalCountValue = 0;
        int totalTimeValue = 0;
        for (var x in state.allExersiceHistory) {
          if (lowetStart == null) {
            lowetStart = x.start;
          }
          if (lowetStart.isAfter(x.start)) {
            lowetStart = x.start;
          }
          if (futurEnd == null) {
            futurEnd = x.end;
          }
          if (!futurEnd.isAfter(x.end)) {
            futurEnd = x.end;
          }
          if (x.type == ExerciesType.count) {
            totalCountValue += int.parse(x.valueAsString());
          } else if (x.type == ExerciesType.time) {
            totalTimeValue += int.parse(x.valueAsString());
          } else {
            throw new Exception("unknown type");
          }
        }
        if (totalCountValue != 0) {
          this.totalReps = totalCountValue;
        }
        if (totalTimeValue != 0) {
          this.totalTime = Duration(milliseconds: totalTimeValue);
        }
      }
    }, child: BlocBuilder<SelectExerciseBloc, SelectExerciseState>(
            builder: (_, state) {
      if (state is LoadingExerciseState) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        var widgets = [
          SizedBox(
            height: 80,
          ),
          Text(
            "All Exercises Stats",
            style: TextStyle(fontSize: 40),
          ),
          Text(
            "Time Exercises",
            style: TextStyle(fontSize: 35),
          ),
          Column(
            children: [
              BigText(
                "Total secends",
                fontSize: 30,
              ),
              BigText(
                totalTime != null ? totalTime.inSeconds.toString() : "0",
                fontSize: 30,
              )
            ],
          ),
          Text(
            "Count Exercises",
            style: TextStyle(fontSize: 35),
          ),
          Column(
            children: [
              BigText(
                "Total reps",
                fontSize: 30,
              ),
              BigText(
                totalReps != null ? totalReps.toString() : "0",
                fontSize: 30,
              ),
            ],
          ),
          Column(
            children: [
              BigText(
                "The first workout: ",
                fontSize: 30,
              ),
              lowetStart != null
                  ? BigText(
                      HistoryWidget.parseDate(lowetStart),
                      fontSize: 30,
                    )
                  : SizedBox(),
            ],
          ),
          Column(
            children: [
              BigText(
                "The last workout: ",
                fontSize: 30,
              ),
              futurEnd != null
                  ? BigText(
                      HistoryWidget.parseDate(futurEnd),
                      fontSize: 30,
                    )
                  : SizedBox()
            ],
          ),
        ];

        return Column(children: widgets);
      }
    }));
  }
}
