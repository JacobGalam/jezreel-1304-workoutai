import 'package:flutter/material.dart';

class _Point {
  double x, y;
  _Point(this.x, this.y);
}

class BndBox extends StatelessWidget {
  final List<dynamic> results;
  final int imageHeight;
  final int imageWidth;
  final double screenHeight;
  final double screenWidth;

  BndBox(this.results, this.imageHeight, this.imageWidth, this.screenHeight,
      this.screenWidth);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: _renderKeypoints(),
    );
  }

  List<Widget> _renderKeypoints() {
    var lists = <Widget>[];
    results.forEach((re) {
      var list = re["keypoints"].values.map<Widget>((k) {
        var _x = k["x"];
        var _y = k["y"];
        var scaleW, scaleH, x, y;

        double factorX = screenWidth;
        double factorY =
            screenHeight; //(imageHeight / imageWidth * screenWidth);

        // if (screenH / screenW > previewH / previewW) {
        //   scaleW = screenH / previewH * previewW;
        //   scaleH = screenH;
        //   var difW = (scaleW - screenW) / scaleW;
        //   x = (_x - difW / 2) * scaleW;
        //   y = _y * scaleH;
        // } else {
        //   scaleH = screenW / previewW * previewH;
        //   scaleW = screenW;
        //   var difH = (scaleH - screenH) / scaleH;
        //   x = _x * scaleW;
        //   y = (_y - difH / 2) * scaleH;
        // }
        return Positioned(
          left: _x * factorX,
          top: _y * factorY,
          child: Container(
            child: Text(
              "●",
              style: TextStyle(
                color: Color.fromRGBO(37, 213, 253, 1.0),
                fontSize: 12.0,
              ),
            ),
          ),
        );
      }).toList();

      lists..addAll(list);
    });

    return lists;
  }
}
