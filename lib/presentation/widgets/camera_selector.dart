import 'package:flutter/material.dart';
import 'package:aiTrainer/domain/entities/camera_data.dart';
import 'package:aiTrainer/domain/entities/cameras.dart';

class CameraSelector extends StatelessWidget {
  final Cameras cameras;
  final CameraData selectedCamera;

  const CameraSelector(this.cameras, this.selectedCamera);

  @override
  Widget build(BuildContext context) {
    List<DropdownMenuItem> items = new List<DropdownMenuItem>();
    if (this.cameras != null) {
      var camerasList = this.cameras.cameras;
      for (var i = 0; i < camerasList.length; i++) {
        items.add(DropdownMenuItem(
          child: Text(camerasList[i].cameraName),
          value: camerasList[i],
        ));
      }
    }
    return DropdownButton(
        value: this.selectedCamera,
        onChanged: (value) {
          // BlocProvider.of<RecognizeBloc>(context)..add(UserSelectCamera(value));
        },
        items: items);
  }
}
