import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:aiTrainer/domain/entities/exercise_data.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/presentation/blocs/select_exercise_bloc/select_exercise_bloc.dart';
import 'package:aiTrainer/presentation/pages/exersice_more_info_page.dart';

class ExerciseItem extends StatefulWidget {
  ExerciseData exerciseData;

  ExerciseItem({
    Key key,
    @required this.exerciseData,
  }) : super(key: key);

  @override
  _ExerciseItemState createState() => _ExerciseItemState();
}

class _ExerciseItemState extends State<ExerciseItem> {
  void moveToMoreInfo() {
    var moreInfoPage = new ExerciseMoreInfoPage(widget.exerciseData);
    var selectBloc = BlocProvider.of<SelectExerciseBloc>(context);
    print("press!");
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => BlocProvider.value(
              value: selectBloc,
              child: moreInfoPage,
            )));
    //selectBloc.add(GetExersiceHistoryEvent(widget.exerciseData));
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          elevation: 3.0,
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height / 1.2,
                    width: MediaQuery.of(context).size.width,
                    child: ClipRRect(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                        ),
                        child: Hero(
                          tag: widget.exerciseData.name,
                          child: Image.network(
                            widget.exerciseData.imagePath,
                            fit: BoxFit.cover,
                          ),
                        )),
                  ),
                  Positioned(
                    top: 6.0,
                    right: 6.0,
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4.0)),
                    ),
                  ),
                  Positioned(
                    top: 6.0,
                    left: 6.0,
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(3.0)),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 7.0),
              Padding(
                padding: EdgeInsets.only(left: 15.0),
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: [
                        Text(
                          widget.exerciseData.name,
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                        Spacer(),
                        Icon(
                            widget.exerciseData.type == ExerciesType.time
                                ? Icons.timer
                                : Icons.plus_one,
                            size: 22),
                      ],
                    )

                    // child: Text(
                    //   "${widget.exerciseData.name}",
                    //   style: TextStyle(
                    //     fontSize: 20.0,
                    //     fontWeight: FontWeight.w800,
                    //   ),
                    //   textAlign: TextAlign.left,
                    // ),
                    ),
              ),
              SizedBox(height: 14.0),
              Padding(
                padding: EdgeInsets.only(left: 15.0),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    "${widget.exerciseData.info}",
                    style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10.0),
            ],
          ),
        ),
        new Positioned.fill(
          child: new Material(
            color: Colors.transparent,
            child: new InkWell(
              onTap: moveToMoreInfo,
            ),
          ),
        )
      ],
    );
  }
}
