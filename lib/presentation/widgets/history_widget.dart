import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BigText extends StatelessWidget {
  String text;
  double fontSize = 35;

  BigText(this.text, {this.fontSize = 35});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(fontSize: fontSize),
    );
  }
}

class HistoryWidget extends StatelessWidget {
  DateTime start;
  DateTime end;
  String value;
  bool isTime;
  bool haveHeader = true;

  HistoryWidget(this.start, this.end, this.value, this.isTime,
      {this.haveHeader = true});

  static String parseDate(DateTime date) {
    final f = new DateFormat('yyyy-MM-dd HH:mm');
    return f.format(date);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              "Stats",
              style: TextStyle(fontSize: 50),
            ),
            Column(
              children: [
                BigText(this.isTime ? "Total Secends:" : "Reps: "),
                BigText(this?.value.toString()),
              ],
            ),
            Column(
              children: [
                BigText("The first workout: "),
                this.start != null
                    ? BigText(parseDate(this.start))
                    : SizedBox(),
              ],
            ),
            Column(
              children: [
                BigText("The last workout: "),
                this.end != null ? BigText(parseDate(this.end)) : SizedBox()
              ],
            ),
          ],
        ),
      ),
    );
  }
}
