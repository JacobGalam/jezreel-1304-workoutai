import 'package:flutter/material.dart';
import 'package:aiTrainer/presentation/widgets/bndbox.dart';

class ImagePose extends StatefulWidget {
  ImagePose(this.image, this.results, this.previewH, this.previewW,
      this.screenH, this.screenW);

  final Image image;
  final List<dynamic> results;
  final int previewH;
  final int previewW;
  final double screenH;
  final double screenW;

  @override
  _ImagePoseState createState() => _ImagePoseState();
}

class _ImagePoseState extends State<ImagePose> {
  var myChildSize = Size.zero;

  @override
  Widget build(BuildContext context) {
    var imageContainer = widget.image;
    var box = widget.results != null
        ? Container(
            child: BndBox(
            widget.results,
            widget.previewH,
            widget.previewW,
            widget.screenH,
            widget.screenW,
          ))
        : Container();
    //var colorBox = new ColoredBox(widget.screenH, widget.screenW);
    return Stack(children: [imageContainer, box]);
  }
}
