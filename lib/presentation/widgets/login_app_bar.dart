import 'package:flutter/material.dart';

class LoginAppBar extends StatelessWidget {
  final String username;
  final String url;

  const LoginAppBar({
    @required this.username,
    @required this.url,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {}
}
