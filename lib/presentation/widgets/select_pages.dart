import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:aiTrainer/domain/entities/exercise_data.dart';
import 'package:aiTrainer/domain/entities/exersice_count_history.dart';
import 'package:aiTrainer/domain/entities/exersice_history.dart';
import 'package:aiTrainer/domain/entities/exersice_time_history.dart';
import 'package:aiTrainer/presentation/blocs/login_bloc/login_bloc.dart';
import 'package:aiTrainer/presentation/blocs/select_exercise_bloc/select_exercise_bloc.dart';
import 'package:aiTrainer/presentation/blocs/upload_video_bloc/upload_video_bloc.dart';
import 'package:aiTrainer/presentation/pages/select_exercise_page.dart';
import 'package:aiTrainer/presentation/pages/upload_video_page.dart';
import 'package:aiTrainer/presentation/widgets/all_history_widget.dart';
import 'package:aiTrainer/presentation/widgets/login_app_bar.dart';

class SelectPages extends StatefulWidget {
  void Function() updateExersices;

  SelectPages(this.updateExersices);

  @override
  _SelectPagesState createState() => _SelectPagesState();
}

class _SelectPagesState extends State<SelectPages> {
  int _selectedIndex = 0;

  bool isLogin = false;
  bool isLoading = false;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void didChangeDependencies() {
    BlocProvider.of<LoginBloc>(context)..add(InitEvent());
    super.didChangeDependencies();
  }

  List<Widget> buildPages() {
    return <Widget>[
      BlocProvider<SelectExerciseBloc>.value(
        value: BlocProvider.of<SelectExerciseBloc>(context),
        child: new ExercisePage(),
      ),
      BlocProvider<SelectExerciseBloc>.value(
        value: BlocProvider.of<SelectExerciseBloc>(context),
        child: new MyExercisePage(),
      ),
      BlocProvider<SelectExerciseBloc>.value(
        value: BlocProvider.of<SelectExerciseBloc>(context),
        child: AllHistoryWidget(),
      ),
      UploadVideoPage(),
    ];
  }

  AppBar buildLogoutAppBar() {
    return AppBar(
      title: Row(
        children: [
          // Image.network(
          //   url,
          //   fit: BoxFit.contain,
          //   height: 32,
          // ),
          Container(padding: const EdgeInsets.all(8.0), child: Text("Login"))
        ],
      ),
      actions: <Widget>[
        Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: () {},
              child: Icon(
                Icons.login,
                size: 26.0,
              ),
            )),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    print(Colors.lightBlue.shade800);
    return MultiBlocListener(
        listeners: [
          BlocListener<LoginBloc, LoginState>(
            listener: (_, state) {
              if (state is LogoutState) {
                isLogin = false;
                isLoading = false;
              } else if (state is LoginState) {
                isLogin = true;
                isLoading = false;
              } else if (state is LoadingState) {
                isLoading = true;
              }
            },
          ),
        ],
        child: Scaffold(
          body: Center(
            child: buildPages().elementAt(_selectedIndex),
          ),
          appBar: AppBar(
            title: BlocBuilder<LoginBloc, LoginState>(builder: (_, state) {
              if (state is LogoutState) {
                return Text("Login -> ");
              } else if (state is LoadingState) {
                return CircularProgressIndicator();
              } else if (state is CanLoginState) {
                return Row(
                  children: [
                    Image.network(
                      state.usernameData.urlIcon,
                      fit: BoxFit.contain,
                      height: 32,
                    ),
                    Container(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(state.usernameData.username))
                  ],
                );
              }
            }),
            actions: isLoading
                ? null
                : <Widget>[
                    Padding(
                        padding: EdgeInsets.only(right: 20.0),
                        child: GestureDetector(
                          onTap: () {
                            if (isLogin) {
                              BlocProvider.of<LoginBloc>(context)
                                ..add(TryLogoutEvent());
                            } else {
                              BlocProvider.of<LoginBloc>(context)
                                ..add(TryLoginEvent());
                            }
                          },
                          child: Icon(
                            isLogin ? Icons.login : Icons.logout,
                            size: 26.0,
                          ),
                        )),
                  ],
          ),
          bottomNavigationBar: MultiBlocProvider(
            child: BottomNavigationBar(
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.fitness_center),
                  title: Text('Exercises'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person),
                  title: Text('My exercises'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.import_contacts),
                  title: Text('Stats'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.add_circle_outline),
                  title: Text('Upload video'),
                ),
              ],
              currentIndex: _selectedIndex,
              selectedItemColor: Colors.lightBlue.shade800,
              onTap: _onItemTapped,
            ),
            providers: [
              BlocProvider<SelectExerciseBloc>.value(
                value: BlocProvider.of<SelectExerciseBloc>(context),
              ),
              BlocProvider<UploadVideoBloc>.value(
                value: BlocProvider.of<UploadVideoBloc>(context),
              )
            ],
          ),
        ));
  }
}
