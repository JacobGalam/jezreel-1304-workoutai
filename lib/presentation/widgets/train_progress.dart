import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:aiTrainer/presentation/blocs/train_bloc/train_bloc.dart';

class TrainProgress extends StatefulWidget {
  TrainProgress({Key key}) : super(key: key);

  @override
  _TrainProgressState createState() => _TrainProgressState();
}

class _TrainProgressState extends State<TrainProgress> {
  String value;

  @override
  Widget build(BuildContext context) {
    return BlocListener<TrainBloc, TrainState>(
        listener: (context, state) {
          if (state is NewTrainState) {
            setState(() {
              value = state.value;
            });
          }
        },
        child: this.value != null
            ? Text(
                this.value.toString(),
                style: TextStyle(fontSize: 70),
              )
            : Container());
  }
}
