import 'package:flutter/cupertino.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/domain/repositories/train_exercise.dart';

class ExerciseTestData {
  TrainExercise exercise;
  String videoPath;
  String name;
  int videoAddHeight = 0;
  int videoAddWidth = 0;
  int screenVideoHeight = 200;
  int screenVideoWidth;
  bool resetSliders;
  ExerciesType type;
  String part;

  ExerciseTestData({
    @required this.exercise,
    @required this.videoPath,
    @required this.type,
    @required this.part,
    this.name,
    this.screenVideoHeight = 200,
    this.screenVideoWidth,
    this.resetSliders = true,
  }) {
    if (name == null) {
      name = this.videoPath;
    }
  }
}

abstract class AbsExerciseTest {
  ExerciseTestData data;

  String getGoal();
}

class TimeExerciseTest implements AbsExerciseTest {
  ExerciseTestData exerciseTestData;
  int goalSecends;

  TimeExerciseTest({
    @required this.data,
    @required this.goalSecends,
  });

  @override
  ExerciseTestData data;

  @override
  String getGoal() {
    return this.goalSecends.toString();
  }
}

class CounterExerciseTest implements AbsExerciseTest {
  int reps;

  @override
  ExerciseTestData data;

  CounterExerciseTest({@required this.reps, @required this.data});

  @override
  String getGoal() {
    return this.reps.toString();
  }
}

class TestConfig {
  List<AbsExerciseTest> tests;
  Duration timeSplit;
  int screenVideoHeightDefult;
  int screenVideoWidthDefult;
  bool printWhenStateChanged;
  bool printInferenceTook; // TODO:?

  TestConfig({
    @required this.tests,
    this.screenVideoHeightDefult,
    this.screenVideoWidthDefult,
    this.timeSplit,
    this.printWhenStateChanged = true,
    this.printInferenceTook = false,
  }) {
    if (this.timeSplit == null) {
      this.timeSplit = new Duration(milliseconds: 400);
    }
  }
}
