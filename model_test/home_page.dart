import 'dart:async';
import 'dart:io';
import 'package:aiTrainer/data/models/posenet_regeneration.dart';
import 'package:aiTrainer/domain/entities/key_points.dart';
import 'package:aiTrainer/domain/entities/on_new_image_data.dart';
import 'package:aiTrainer/domain/entities/recognizing.dart';
import 'package:aiTrainer/domain/entities/rect.dart';
import 'package:aiTrainer/presentation/widgets/image_item.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tflite/tflite.dart';
import 'tester.dart';
import 'video.dart';
import 'package:export_video_frame/export_video_frame.dart';
import 'package:flutter/rendering.dart';

import 'package:flutter/material.dart';
import 'ui_helpers.dart';
import 'config.dart';
import 'video_ai.dart';

class MyHomePage extends StatefulWidget {
  final TestConfig config;

  MyHomePage({Key key, @required this.config}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ImageData image;
  double screenH = 0;
  double screenW = 0;
  int imageHAdd = 0;
  int imageWAdd = 0;
  int testIndex = 0;
  var flosts = new List<double>();

  var _isClean = false;
  var _initModel = false;
  Tester tester;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Testing"),
      ),
      body: Container(
        padding: EdgeInsets.zero,
        child: Column(
          children: <Widget>[
            Expanded(
                flex: 1,
                child: image != null ? buildImage(image) : Container()),
            Expanded(
              flex: 0,
              child: Center(
                  child: _initModel
                      ? MaterialButton(
                          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          height: 40,
                          minWidth: 150,
                          onPressed: () {
                            if (_initModel) _handleClickSecond();
                          },
                          color: Colors.orange,
                          child: Text(_isClean ? "Clean" : "Test"),
                        )
                      : Container()),
            ),
            Expanded(
                flex: 0,
                child: MySilder(
                  "screenH",
                  600,
                  (value) => setState(() => this.screenH = value),
                  value: this.screenH,
                )),
            Expanded(
                flex: 0,
                child: MySilder(
                  "screenW",
                  600,
                  (value) => setState(() => this.screenW = value),
                  value: this.screenW,
                )),
            Expanded(
                flex: 0,
                child: MySilder(
                  "imageHAdd",
                  600,
                  (value) => setState(() => this.imageHAdd = value.toInt()),
                  min: -600,
                  value: this.imageHAdd.toDouble(),
                )),
            Expanded(
                flex: 0,
                child: MySilder(
                  "imageWAdd",
                  600,
                  (value) => setState(() => this.imageWAdd = value.toInt()),
                  min: -600,
                  value: this.imageWAdd.toDouble(),
                )),
            Expanded(
              flex: 0,
              child: Center(),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    asyncMethod();
  }

  Widget buildImage(ImageData data) {
    return Container(child: LayoutBuilder(builder: (_, constraints) {
      var i = ImageItem(
        data.image,
        data.results,
        data.videoHeight + this.imageHAdd,
        data.videoWidth + this.imageWAdd,
        this.screenH,
        this.screenW,
      );
      return i;
    }));
  }

  void asyncMethod() async {
    print(await Tflite.loadModel(
        model: "assets/posenet_mv1_075_float_from_checkpoints.tflite",
        numThreads: 4, // defaults to 1
        useGpuDelegate: false));

    setState(() {
      _initModel = true;
    });
  }

  Future _handleClickSecond() async {
    if (_isClean) {
      await _cleanCache();
    } else {
      setState(() {
        _isClean = true;
      });
      await _startTesting();
    }
  }

  Future _cleanCache() async {
    var result = await ExportVideoFrame.cleanImageCache();
    print(result);
    setState(() {
      _isClean = false;
    });
  }

  Future _startTesting() async {
    for (var test in widget.config.tests) {
      int s = await startTest(test.data, test.getGoal());
    }
  }

  void onReadImage(OnNewImageData dataImage) {
    ImageData data = new ImageData(
      Image.file(dataImage.image),
      dataImage.results != null ? dataImage.results.raw : [],
      dataImage.videoHeight,
      dataImage.videoWidth,
    );
    setState(() {
      this.image = data;
    });
  }

  Future<int> startTest(ExerciseTestData test, String goal) async {
    var file = await getImageFileFromAssets(test.videoPath);
    this.tester = new Tester(test, widget.config.printWhenStateChanged, goal);
    var vai = VideoAI(test.exercise, onReadImage);

    setState(() {
      screenH = test.screenVideoHeight.toDouble();
      screenW = test.screenVideoWidth.toDouble();
    });

    var exerciseValue = await vai.readVideo(
      part: test.part,
      type: test.type,
      videoFile: file,
      value: goal,
    );
    tester.accuracy = await exerciseValue.getAccuracy();
    this.tester.onFinish();

    var t = exerciseValue.exerciseValue;
    print(
        '${t.down.toStringAsFixed(2)}, ${t.up.toStringAsFixed(2)}, "${t.part}"');
    flosts.clear();
    return 69;
  }
}
