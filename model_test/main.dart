import 'package:flutter/material.dart';
import 'package:aiTrainer/domain/entities/dynamic_exercise.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/domain/entities/exercise_value.dart';
import 'package:aiTrainer/domain/repositories/train_count_exercise.dart';
import 'package:aiTrainer/domain/repositories/train_exercise.dart';
import 'package:aiTrainer/domain/repositories/train_time_exercise.dart';
import 'config.dart';
import 'home_page.dart';

TrainCountExercise buildCountExerciseValues(num down, num up, String angle) {
  return TrainCountExercise(
      onNewState: (a) => {},
      dynamicCountExercise: DynamicExercise(
          exerciseValue: ExerciseValue(down: down, up: up, part: angle)));
}

TrainTimeExercise buildTimeExerciseValues(num down, num up, String angle) {
  return TrainTimeExercise(
      onNewState: (a) => {},
      dynamicCountExercise: DynamicExercise(
          exerciseValue: ExerciseValue(down: down, up: up, part: angle)));
}

AbsExerciseTest buildTimeTrainTest({
  @required int secendsGoal,
  @required String videoPath,
  @required num down,
  @required num up,
  @required String angle,
  int screenVideoHeight = 270,
  int screenVideoWidth = 150,
}) {
  return TimeExerciseTest(
    goalSecends: secendsGoal,
    data: ExerciseTestData(
      exercise: buildTimeExerciseValues(down, up, angle),
      videoPath: videoPath,
      screenVideoHeight: 270,
      screenVideoWidth: 150,
      part: angle,
      type: ExerciesType.time,
    ),
  );
}

AbsExerciseTest buildCountTrainTest({
  @required int reps,
  @required String videoPath,
  @required num down,
  @required num up,
  @required String angle,
  int screenVideoHeight = 270,
  int screenVideoWidth = 150,
}) {
  return CounterExerciseTest(
    reps: reps,
    data: ExerciseTestData(
      exercise: buildCountExerciseValues(down, up, angle),
      videoPath: videoPath,
      screenVideoHeight: 270,
      screenVideoWidth: 150,
      part: angle,
      type: ExerciesType.count,
    ),
  );
}

/*
  "Neck",
  "Right Shoulder",
  "Left Shoulder",
  "Right Chest",
  "Left Chest",
  "Right Elbow",
  "Left Elbow",
  "Right Hip",
  "Left Hip",
  "Right Knee",
  "Left Knee",
  */

var tests = [
  buildTimeTrainTest(
    secendsGoal: 8,
    videoPath: "exercises/barMazal.3gp",
    angle: "Left Elbow",
    down: 28.20,
    up: 174.69,
  ),
  buildTimeTrainTest(
    secendsGoal: 12,
    videoPath: "exercises/barJacob.mp4",
    angle: "Left Elbow",
    down: 28.20,
    up: 174.69,
  ),

  // buildCountTrainTest(
  //   reps: 5,
  //   videoPath: "exercises/a.3gp",
  //   angle: "Left Knee",
  //   down: 66.68,
  //   up: 114.69,
  // ),
  // buildCountTrainTest(
  //   reps: 6,
  //   videoPath: "exercises/b2.mp4",
  //   angle: "Left Knee",
  //   down: 66.68,
  //   up: 114.69,
  // ),
  // buildTimeTrainTest(
  //   secendsGoal: 25,
  //   videoPath: "exercises/time.3gp",
  //   angle: "Left Knee",
  //   down: 39.33,
  //   up: 168.07,
  // ),
];

TestConfig config = new TestConfig(
  tests: tests,
);

void main() async {
  // for (var i = 1; i < 12; i++) {
  //   tests.add(new ExerciseTest(
  //     exercise: Squat(),
  //     goal: "5",
  //     vidoePath: "exercises/$i.mp4",
  //     screenVideoHeight: 270,
  //     screenVideoWidth: 150,
  //   ));
  // }
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  bool hasSetVidoeWidth = false;

  MyApp();

  @override
  Widget build(BuildContext context) {
    // if (!hasSetVidoeWidth) {
    //   setVidoeWidth(context);
    //   hasSetVidoeWidth = true;
    // }

    return MaterialApp(
      title: "Plugin Example App",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(config: config),
    );
  }
}
