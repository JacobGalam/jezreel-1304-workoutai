import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/domain/entities/recognizing.dart';
import 'package:aiTrainer/domain/repositories/train_exercise.dart';

import 'config.dart';

class Tester {
  TrainExercise exercise;
  ExerciseTestData data;
  String name;
  bool printWhenStateChanged;
  String stateLastTime = "";
  double accuracy;
  String goal;

  Tester(ExerciseTestData test, bool printWhenStateChanged, this.goal) {
    exercise = test.exercise;
    data = test;
    name = test.name;
    this.printWhenStateChanged = printWhenStateChanged;
  }

  String timeWithoutMilisec() {
    return DateTime.now().toString().split(".")[0];
  }

  void _log(String msg) {
    print("${timeWithoutMilisec()}: $name: $msg");
  }

  void load(Recognizing recognizing) {
    if (recognizing == null) return;
    this.exercise.addRecognizing(recognizing);

    if (compereTolastTime()) {
      stateLastTime = stateAsString();
      if (printWhenStateChanged) {
        _log("New state: ${stateAsString()}");
      }
    }
  }

  void onFinish() {
    if (_compereToGoal()) {
      _log("test PASS: ${stateAsString()}");
    } else {
      _log("test FAILED: return ${stateAsString()} and not ${goalAsString()}");
    }
  }

  bool _compereToGoal() {
    return stateAsString() == goalAsString();
  }

  bool compereTolastTime() {
    return stateLastTime == stateAsString();
  }

  String stateAsString() {
    return this.exercise.getComperableState();
  }

  String goalAsString() {
    return goal;
  }
}
