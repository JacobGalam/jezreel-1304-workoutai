import '../lib/presentation/widgets/bndbox.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:math' as math;

class ColoredBox extends StatelessWidget {
  final double height;
  final double width;

  ColoredBox(this.height, this.width);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      color: Colors.red,
    );
  }
}

class ImageData {
  Image image;
  List<dynamic> results;
  int videoHeight;
  int videoWidth;
  ImageData(
    this.image,
    this.results,
    this.videoHeight,
    this.videoWidth,
  );
}

class MySilder extends StatelessWidget {
  final String name;
  final double size;
  final Function(double) newSizeCallback;
  final double min;
  final double value;

  MySilder(
    this.name,
    this.size,
    this.newSizeCallback, {
    this.min = 0,
    this.value = 0,
  });

  @override
  Widget build(BuildContext context) {
    if (newSizeCallback == null) {
      return Container();
    }
    var slider = Slider(
      value: value,
      min: min,
      max: size,
      divisions: size.toInt() + min.toInt().abs(),
      label: value.round().toString(),
      onChanged: (double value) {
        newSizeCallback(value);
      },
    );
    var text = Text(
      name,
      style: TextStyle(fontSize: 20),
    );
    return Column(children: [text, slider]);
  }
}
