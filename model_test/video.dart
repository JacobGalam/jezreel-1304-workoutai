import 'dart:async';
import 'dart:io';
import 'package:export_video_frame/export_video_frame.dart';
import 'dart:math';

import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart' show rootBundle;

import 'package:media_info/media_info.dart';
import 'package:path/path.dart';

Future<File> getImageFileFromAssets(String path) async {
  final byteData = await rootBundle.load('assets/$path');
  final fileName = basename(path);

  final file = new File('${(await getTemporaryDirectory()).path}/$fileName');
  await file.writeAsBytes(byteData.buffer
      .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

  return file;
}

class VideoSplitter {
  File video;
  Duration interval;
  Duration videoLength;
  int videoHeight;
  int videoWidth;

  VideoSplitter(this.video, this.interval);

  Future<void> loadVidepMetadata() async {
    var mediaInfo = MediaInfo();
    var videoInfo = await mediaInfo.getMediaInfo(video.path);

    videoHeight = videoInfo['height'];
    videoWidth = videoInfo['width'];
    videoLength = Duration(milliseconds: videoInfo["durationMs"]);
  }

  Stream<Duration> _loopOverDurationsOfVideo() async* {
    for (var i = Duration.zero; i < videoLength; i += interval) {
      yield i;
    }
  }

  Stream<File> getImages() async* {
    var durations = _loopOverDurationsOfVideo();
    await for (var duration in durations) {
      yield await ExportVideoFrame.exportImageBySeconds(
          video, duration, pi / 2);
    }
  }
}
