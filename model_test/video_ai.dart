import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:aiTrainer/data/datasources/posenet_souce.dart';
import 'package:aiTrainer/data/repositories/video_ai_read_imp.dart';
import 'package:aiTrainer/data/repositories/video_metadata_reader_imp.dart';
import 'package:aiTrainer/data/repositories/video_splitter_imp.dart';
import 'package:aiTrainer/domain/entities/exercise_type.dart';
import 'package:aiTrainer/domain/entities/on_new_image_data.dart';
import 'package:aiTrainer/domain/entities/processed_count_exercise.dart';
import 'package:aiTrainer/domain/entities/processed_exercise.dart';
import 'package:aiTrainer/domain/entities/processed_time_exercise.dart';
import 'package:aiTrainer/domain/entities/recognizing.dart';
import 'package:aiTrainer/domain/entities/video_metadata.dart';
import 'package:aiTrainer/domain/repositories/train_exercise.dart';
import 'package:aiTrainer/domain/repositories/video_ai_read.dart';

const Duration SPACE = Duration(milliseconds: 400);

class TestVideoAiReadImp extends VideoAiReadImp {
  PosenetSource posenetSource;
  TrainExercise exercise;
  TestVideoAiReadImp(this.posenetSource, this.exercise) : super(posenetSource);

  @override
  Future<File> readVideo(VideoMetadata metadata, Stream<File> images,
      int numberOfImages, String bodyPart, onfind) async {
    // TODO: implement readVideo
    int i = 1;
    File firstFrame;
    await for (File image in images) {
      if (firstFrame == null) {
        firstFrame = image;
      }
      int height = metadata.videoHeight;
      int width = metadata.videoWidth;
      // TODO: use dataSource
      var posenetRegeneration =
          await this.posenetSource.recognizeFile(image.path);

      Recognizing recognizing = posenetRegeneration;
      if (posenetRegeneration != null) {
        var angleValue = recognizing.getByName(bodyPart);

        exercise.addRecognizing(recognizing);

        values.add(angleValue);
      }
      if (onfind != null) {
        onfind(OnNewImageData(
          image: image,
          present: i / numberOfImages,
          results: recognizing?.keyPointsModel,
          videoHeight: height,
          videoWidth: width,
        ));

        i++;
      }
    }

    return firstFrame;
  }
}

class VideoAI {
  TrainExercise exercise;
  VideoSplitterImp videoSplitter = new VideoSplitterImp();
  VideoMetadataReaderImp videoMetadataReader = new VideoMetadataReaderImp();

  OnFound onfind;

  VideoMetadata _metadata;

  VideoAI(this.exercise, this.onfind);

  Future<ProcessedExerciseAbs> readVideo({
    @required File videoFile,
    @required String part,
    @required ExerciesType type,
    String value = "",
  }) async {
    TestVideoAiReadImp videoAiRead = new TestVideoAiReadImp(
      new PosenetSourceImp(),
      this.exercise,
    );

    var videoPath = videoFile.path;
    await videoAiRead.init();
    this._metadata = await videoMetadataReader.getMetadata(videoPath);
    int numberOfImages =
        _metadata.videoLength.inMilliseconds ~/ SPACE.inMilliseconds;
    var imageStream = videoSplitter.getImages(videoFile, SPACE, this._metadata);

    if (type == ExerciesType.time) {
      return await videoAiRead.readVideoTime(
        this._metadata,
        imageStream,
        numberOfImages,
        part,
        this.onfind,
      );
    } else if (type == ExerciesType.count) {
      return await videoAiRead.readVideoCount(
        int.parse(value),
        this._metadata,
        imageStream,
        numberOfImages,
        part,
        this.onfind,
      );
    } else {
      throw new Exception("unknown exercise type");
    }
  }

  VideoMetadata getMetadata() {
    return _metadata;
  }
}
