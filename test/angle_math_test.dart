import 'package:aiTrainer/domain/entities/angle_math.dart';
import 'package:aiTrainer/domain/entities/key_points.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Check angle of triangle', () {
    KeyPointModel a = KeyPointModel(6, 6);
    KeyPointModel b = KeyPointModel(8, 10);
    KeyPointModel c = KeyPointModel(4, 9);

    double aAngle = calculateAngle(a, b, c);
    double bAngle = calculateAngle(b, a, c);
    double cAngle = calculateAngle(c, b, a);

    expect(aAngle, closeTo(60.255, 0.01));
    expect(bAngle, closeTo(49.399, 0.01));
    expect(cAngle, closeTo(70.346, 0.01));
  });
}
